# CoursesWeb

The project is modelled after the case description found at [here](#case-description-for-modeling-assignments). The most important parts in the project are an _Ecore_ model, a _genmodel_ and an instance of the model (in _.xmi_ format) which can be found in the __./model__ directory. The instance is modelled after the course pages of [TDT4250](https://www.ntnu.edu/studies/courses/TDT4250/2018#tab=omEmnet) and [TDT4100](https://www.ntnu.edu/studies/courses/TDT4100/2018#tab=omEmnet). The instance is not a complete copy of these pages though. For instance, the credit reductions of different courses are not included, though as an example, a credit reduction is added refering to the other course in the instance. Furthermore, a similar example can be seen for the recommended and required courses.

## Classes
The structure for a model instance includes the `University` class as the root. A `University` object can contain multiple object of the `Course`, `Department`, `StudyProgram`, `Room` and `Person` classes. The `Person`, `Department`, `StudyProgram` and `Room` classes are mainly used to support the `Course` class. These classes have a small structure (e.g. a `Person` has a first name and a surname, and a `StudyProgram` just has a code and a list of courses which it provides). To a `University` object, employees can be included as `Person` objects and vice versa for the departments (e.g. IDI - Department of Computer Science), rooms (e.g. K5 Kjemi 5) and study programs that a university has.

A `Course` instance has a unique code, a name and content (giving a short introduction into the course). Furthermore, it has credits which a student will receive after completing the course successfully. Also a `Course` instance has a `CourseWork` object. This object has the number lecture and lab hours that will be spend on the course in a week (in fact more hours, but at least this amount, can be scheduled in the timetable).

Besides, a `Course` instance contains multiple `CourseInstance` objects. These represent the semester-specific data collected for courses, such as the timetable. `CourseInstance` objects have a responsible department, staff, a timetable, and a year and semester (either Autumn or Spring; from the `ESemester` enum). Furthermore, it has an opposite-reference to the `Course` object it is contained in. A single staff member is represented by a role (either course coordinator, lecturer, or an other role; from the `EStaffRole` enum) which has a reference to a `Person` object who takes this role. Therefore, the `Person` object is typically used to represent employees of the university. Furthermore, instances of the `TimetableEntry` class together form a timetable for a `CourseInstance`. They contain the information for when lectures and lab sessions are hold for a course. These entries have a weekday (from the `EWeekDay` enum), a class type (either a lecture or a lab; from the `EClassType` enum), the room for the session, the study programs that take part in this session, and the timeslot during which the session takes place (e.g. 10:15-12:00).

Coming back to the `Course` class, objects of this class contain, besides the `CourseInstance` objects, also may have recommended and/or required courses. A course can also have defined credit reductions on the provided credits for some courses that a student might already have taken. These are represented by the `CreditReduction` objects. Each such an object has an amount of reduction (at most the maximum possible credits for the course), the course that, if taken by a student, applies this credit reduction (referred to as the `byCourse`), and the course on which the reduction is applied (the `onCourse`). Lastly, a course will have multiple assignments, project and exams which the students have to do. These are included in the evaluation form of a `Course` instance. The entries in this form, represented by the `EvaluationFormEntry` class, have a name/description of the work (such as _Written examination_), and a weighting. All entries together should sum up to a weighting of 100.

## Constraints
There are a few constraints to which an instance of the model should conform. These constraints can be used to check the instance for validity. There are a few trivial, but necessarry, constraints, such as non-negative hours, credits, etc. Furthermore, the weighting, credit reductions and timeslots should be within range. This means that a single weighting of work can't go past 100, the credit reductions can be maximally the course's credits itself, and the timeslots must correspond to the 24 hour ranges (i.e. 26:00 or 13:65 are not within range). Also the codes for courses, departments and study programs should be unique.

Furthermore, a recommended and required courses in a `Course` instance should refer to a different instance. The same holds for the credit reductions (the `onCourse` and `byCourse` should be different instances). Besides, `CourseInstance` objects, of the same course,  can't have the same year-semester pair (i.e. if the year is the same, the semester should be different and vice versa). When summing the weightings in the evaluation form entries, the result should of course be 100. Also a course instance should always have one course coordinator (no more, no less), and a staff member can't have duplicate roles (i.e. an employee can't be twice a lecturer in the same course instance).

The most complex constraints are constraints on the timetable. These include that the study programs which are referred in a timetable entry should provide the specific course. Furthermore, the timetable entries should not interfer. Moreover, they should not be during the same timeslot (or interfering timeslots) on the same day for one or more of the same study programs. Finally, the scheduled hours in the timetable should correspond to the lecture and lab hours in the `CourseWork` object (actually, they should be at least this much). 

## Case description for modeling assignments
This case description provides a context for several assignments, where the overall goal is to explore the various features of EMF in general and Ecore in particular. The domain is administrative information about courses held at NTNU, based on the official course web site, e.g. TDT4250, TDT4100 and All courses, and the goal is to be able to generate (using some kind of model transformation) these web pages (essentially the tables). The domain is pretty large and complex, so we'll limit the scope and hence (the contents of the) web pages that must covered.

### Courses and course instances

Courses are identified by their code, e.g. TDT4250, and their name, e.g. Advanced Software Design. A course consists of pretty stable properties, like code, name and content, and data that are specific for the semesters they are taught, like evaluation form, timetable and staff. The semester-specific properties are gathered in what is often called the course instance (here "instance" should not be confused with an "instance" of a course "class"), so a course consists of a number of course instances, one for each semester the course has been taught. The size of a course is measured in credits, typically multiples of 7.5, which the students receive after taking the exam.

### Organization (contact information)

A course (instance) has a responsible department, e.g. IDI - Department of Computer Science, and at least has a course coordinator and may also have lecturers and other people playing various roles.

### Relation to other courses

Other courses may be required or recommended as preconditions for taking a course. In addition, the number of credits received may be reduced if you already have taken certain other courses, e.g. if you have taken the Java course (TDT4100), you credits for the C++ course (TDT4102) are reduced by 3.7 points.

### Evaluation form

The evaluation form can be a combination of various activities and work, like exam, project and assignments, each with a percentage of the final grade (which of course should sum up to 100). Or, such work may by required without contributing to the grade.

### Coursework

The course has a number of lecture and lab hours that should correspond to the scheduled ones (see Timetable).

### Students and study programs

A course may be part of several study programs (identified by a code), and hence students in these programs may register for a corresponding course (instance) and later take the exam.

### Timetable

The timetable shows the scheduled lecture and lab hours for a course (instance), as well as the room used (we omit the week information). Note that courses with lots of students may split the class, so certain scheduled hours are reserved for certain study programs. The scheduled hours must correctly sum up for each study program. Note that the scheduled hours may exceed those for the course, e.g. this course has added two scheduled lab hours, so the sum of the timetable hours will be more than the Coursework hours. 

