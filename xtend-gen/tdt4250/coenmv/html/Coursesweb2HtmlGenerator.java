package tdt4250.coenmv.html;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import tdt4250.coenmv.coursesweb.Course;
import tdt4250.coenmv.coursesweb.CourseInstance;
import tdt4250.coenmv.coursesweb.CourseWork;
import tdt4250.coenmv.coursesweb.CreditReduction;
import tdt4250.coenmv.coursesweb.Department;
import tdt4250.coenmv.coursesweb.ESemester;
import tdt4250.coenmv.coursesweb.EStaffRole;
import tdt4250.coenmv.coursesweb.EvaluationFormEntry;
import tdt4250.coenmv.coursesweb.Person;
import tdt4250.coenmv.coursesweb.Staff;
import tdt4250.coenmv.coursesweb.StudyProgram;
import tdt4250.coenmv.coursesweb.TimetableEntry;
import tdt4250.coenmv.coursesweb.University;
import tdt4250.coenmv.coursesweb.util.Timeslot;

@SuppressWarnings("all")
public class Coursesweb2HtmlGenerator {
  public String generateHtml(final University university) {
    StringBuilder _stringBuilder = new StringBuilder();
    return this.generateHtml(university, _stringBuilder);
  }
  
  public String generateHtml(final University university, final StringBuilder builder) {
    String _xblockexpression = null;
    {
      this.generatePreHtml(university.getName(), builder);
      int _size = university.getCourses().size();
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _size, true);
      for (final Integer i : _doubleDotLessThan) {
        {
          if (((i).intValue() > 0)) {
            Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<br/><hr/><br/>");
          }
          this.generate(university.getCourses().get((i).intValue()), builder);
        }
      }
      this.generatePostHtml(builder);
      this.generateScript(university.getCourses(), builder);
      this.generateStyle(builder);
      _xblockexpression = builder.toString();
    }
    return _xblockexpression;
  }
  
  public String generateHtml(final Course course) {
    StringBuilder _stringBuilder = new StringBuilder();
    return this.generateHtml(course, _stringBuilder);
  }
  
  public String generateHtml(final Course course, final StringBuilder builder) {
    String _xblockexpression = null;
    {
      this.generatePreHtml(course.getName(), builder);
      this.generate(course, builder);
      this.generatePostHtml(builder);
      this.generateScript(course, builder);
      this.generateStyle(builder);
      _xblockexpression = builder.toString();
    }
    return _xblockexpression;
  }
  
  public void generatePreHtml(final String title, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<html>");
    _builder.newLine();
    _builder.append("<head>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<title>");
    _builder.append(title, "\t");
    _builder.append("</title>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=\"UTF-8\"/>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">");
    _builder.newLine();
    _builder.append("</head>");
    _builder.newLine();
    _builder.append("<body>");
    _builder.newLine();
    _builder.append("<div class=\"container-fluid\">");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public void generatePostHtml(final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("</div>");
    _builder.newLine();
    _builder.append("</body>");
    _builder.newLine();
    _builder.append("<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>");
    _builder.newLine();
    _builder.append("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>");
    _builder.newLine();
    _builder.append("<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>");
    _builder.newLine();
    _builder.append("</html>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generate(final Course course, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<div>");
    _builder.newLine();
    _builder.append("<h2>");
    String _code = course.getCode();
    _builder.append(_code);
    _builder.append(" - ");
    String _name = course.getName();
    _builder.append(_name);
    _builder.append("</h2>");
    _builder.newLineIfNotEmpty();
    _builder.append("<div><h4>Content</h4><p>");
    String _content = course.getContent();
    _builder.append(_content);
    _builder.append("</p></div>");
    _builder.newLineIfNotEmpty();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
    boolean _isEmpty = course.getRecommendedCourses().isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<div><h4>Recommended courses</h4>");
      this.generateCourseTable(course.getRecommendedCourses(), builder);
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
    }
    boolean _isEmpty_1 = course.getRequiredCourses().isEmpty();
    boolean _not_1 = (!_isEmpty_1);
    if (_not_1) {
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<div><h4>Required courses</h4>");
      this.generateCourseTable(course.getRequiredCourses(), builder);
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
    }
    boolean _isEmpty_2 = course.getCreditReductions().isEmpty();
    boolean _not_2 = (!_isEmpty_2);
    if (_not_2) {
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<div><h4>Credit reductions</h4>");
      this.generateCreditReductionsTable(course.getCreditReductions(), builder);
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
    }
    boolean _isEmpty_3 = course.getEvaluationForm().isEmpty();
    boolean _not_3 = (!_isEmpty_3);
    if (_not_3) {
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<div><h4>Evaluation Form</h4>");
      this.generateEvaluationForm(course.getEvaluationForm(), builder);
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
    }
    this.generateFacts(course, builder);
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
    this.generateInstanceSelect(course, builder);
    final Consumer<CourseInstance> _function = (CourseInstance it) -> {
      this.generate(it, builder);
    };
    course.getCourseInstances().forEach(_function);
  }
  
  public void generateFacts(final Course course, final StringBuilder builder) {
    this.generatePreCard(builder, "Facts");
    this.generateCardSubtitle(builder, "Credits");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<p class=\"card-text\">");
    float _credits = course.getCredits();
    _builder.append(_credits);
    _builder.append(" SP</p>");
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
    this.generate(course.getCourseWork(), builder);
    this.generatePostCard(builder);
  }
  
  public void generateInstanceSelect(final Course course, final StringBuilder builder) {
    final EList<CourseInstance> instances = course.getCourseInstances();
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<select class=\"custom-select float-right\" id=\"");
    String _code = course.getCode();
    _builder.append(_code);
    _builder.append("InstanceSelect\" onchange=\"toggleSelection(\'");
    String _code_1 = course.getCode();
    _builder.append(_code_1);
    _builder.append("\', this)\" style=\"width: 12rem;\">");
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
    for (final CourseInstance instance : instances) {
      {
        StringConcatenation _builder_1 = new StringConcatenation();
        String _code_2 = instance.getCourse().getCode();
        _builder_1.append(_code_2);
        _builder_1.append("Instance");
        ESemester _semester = instance.getSemester();
        _builder_1.append(_semester);
        int _year = instance.getYear();
        _builder_1.append(_year);
        final String id = _builder_1.toString();
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("<option value=\"");
        _builder_2.append(id);
        _builder_2.append("\">");
        String _enumToString = Coursesweb2HtmlGenerator.enumToString(instance.getSemester());
        _builder_2.append(_enumToString);
        _builder_2.append(" ");
        int _year_1 = instance.getYear();
        _builder_2.append(_year_1);
        _builder_2.append("</option>");
        _builder_2.newLineIfNotEmpty();
        Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder_2);
      }
    }
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</select>");
  }
  
  protected void _generate(final CourseInstance instance, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<div class=\"");
    String _code = instance.getCourse().getCode();
    _builder.append(_code);
    _builder.append("Instance\" id=\"");
    String _code_1 = instance.getCourse().getCode();
    _builder.append(_code_1);
    _builder.append("Instance");
    ESemester _semester = instance.getSemester();
    _builder.append(_semester);
    int _year = instance.getYear();
    _builder.append(_year);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<h3>Semester data - ");
    String _enumToString = Coursesweb2HtmlGenerator.enumToString(instance.getSemester());
    _builder.append(_enumToString, "\t");
    _builder.append(" ");
    int _year_1 = instance.getYear();
    _builder.append(_year_1, "\t");
    _builder.append("</h3>");
    _builder.newLineIfNotEmpty();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
    this.generateTimetable(instance.getTimetable(), builder);
    this.generatePreCard(builder, "Contact information");
    this.generateStaff(instance.getStaff(), builder);
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<br/>");
    this.generate(instance.getResponsibleDepartment(), builder);
    this.generatePostCard(builder);
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
  }
  
  protected void _generate(final Department department, final StringBuilder builder) {
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<div>");
    this.generateCardSubtitle(builder, "Responsible department");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<p>");
    String _code = department.getCode();
    _builder.append(_code);
    _builder.append(" - ");
    String _name = department.getName();
    _builder.append(_name);
    _builder.append("</p>");
    _builder.newLineIfNotEmpty();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
  }
  
  public void generateStaff(final EList<Staff> staff, final StringBuilder builder) {
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<div>");
    this.generateCardSubtitle(builder, "Staff");
    final Function1<Staff, EStaffRole> _function = (Staff it) -> {
      return it.getRole();
    };
    final Map<EStaffRole, List<Staff>> grouped = IterableExtensions.<EStaffRole, Staff>groupBy(staff, _function);
    Set<Map.Entry<EStaffRole, List<Staff>>> _entrySet = grouped.entrySet();
    for (final Map.Entry<EStaffRole, List<Staff>> entry : _entrySet) {
      {
        EStaffRole _key = entry.getKey();
        final EStaffRole role = ((EStaffRole) _key);
        final List<Staff> employees = entry.getValue();
        Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<div><p class=\"card-text mb-2\">");
        this.generate(role, builder);
        for (final Staff employee : employees) {
          {
            Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "<br/>");
            this.generate(employee.getPerson(), builder);
          }
        }
        Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</p></div>");
      }
    }
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
  }
  
  public void generatePreCard(final StringBuilder builder, final String title) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<div class=\"card mb-4\" style=\"display: inline-block;\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<div class=\"card-body\">");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<h4 class=\"card-title\">");
    _builder.append(title, "\t\t");
    _builder.append("</h4>");
    _builder.newLineIfNotEmpty();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public void generateCardSubtitle(final StringBuilder builder, final String subtitle) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<h6 class=\"card-subtitle mb-2 text-muted\">");
    _builder.append(subtitle);
    _builder.append("</h6>");
    _builder.newLineIfNotEmpty();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public void generatePostCard(final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("</div>");
    _builder.newLine();
    _builder.append("</div>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public void generatePreTable(final StringBuilder builder, final String... headers) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<table class=\"table table-striped table-responsive mb-4 mt-0\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<thead>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<tr>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
    final Consumer<String> _function = (String it) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("<th>");
      _builder_1.append(it);
      _builder_1.append("</th>");
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder_1);
    };
    ((List<String>)Conversions.doWrapArray(headers)).forEach(_function);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("\t");
    _builder_1.append("</tr>");
    _builder_1.newLine();
    _builder_1.append("</thead>");
    _builder_1.newLine();
    _builder_1.append("<tbody>");
    _builder_1.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder_1);
  }
  
  public void generatePostTable(final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("</tbody>");
    _builder.newLine();
    _builder.append("</table>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generate(final CourseWork courseWork, final StringBuilder builder) {
    this.generateCardSubtitle(builder, "Coursework");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<p class=\"card-text\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("Lecture hours: ");
    int _lectureHours = courseWork.getLectureHours();
    _builder.append(_lectureHours, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<br/>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("Lab hours: ");
    int _labHours = courseWork.getLabHours();
    _builder.append(_labHours, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("</p>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public void generateCourseTable(final EList<Course> courses, final StringBuilder builder) {
    this.generatePreTable(builder, "Course code", "Course name");
    final Consumer<Course> _function = (Course it) -> {
      this.generateTr(it, builder);
    };
    courses.forEach(_function);
    this.generatePostTable(builder);
  }
  
  protected void _generate(final Person person, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    String _firstName = person.getFirstName();
    _builder.append(_firstName);
    _builder.append(" ");
    String _lastName = person.getLastName();
    _builder.append(_lastName);
    _builder.newLineIfNotEmpty();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generate(final EStaffRole role, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<i>");
    String _enumToString = Coursesweb2HtmlGenerator.enumToString(role);
    _builder.append(_enumToString);
    _builder.append(":</i>");
    _builder.newLineIfNotEmpty();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generateTr(final Course course, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<tr>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<td>");
    String _code = course.getCode();
    _builder.append(_code, "\t");
    _builder.append("</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<td>");
    String _name = course.getName();
    _builder.append(_name, "\t");
    _builder.append("</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("</tr>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generateTr(final CreditReduction creditReduction, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<tr>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<td>");
    String _code = creditReduction.getByCourse().getCode();
    _builder.append(_code, "\t");
    _builder.append("</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<td>");
    float _reduction = creditReduction.getReduction();
    _builder.append(_reduction, "\t");
    _builder.append("</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("</tr>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generateTr(final EvaluationFormEntry entry, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<tr>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<td>");
    String _type = entry.getType();
    _builder.append(_type, "\t");
    _builder.append("</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<td>");
    int _weighting = entry.getWeighting();
    _builder.append(_weighting, "\t");
    _builder.append("/100</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("</tr>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generateTr(final TimetableEntry entry, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<tr>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<td>");
    String _enumToString = Coursesweb2HtmlGenerator.enumToString(entry.getWeekDay());
    _builder.append(_enumToString, "\t");
    _builder.append("</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<td>");
    Timeslot _timeslot = entry.getTimeslot();
    _builder.append(_timeslot, "\t");
    _builder.append("</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<td>");
    String _enumToString_1 = Coursesweb2HtmlGenerator.enumToString(entry.getClassType());
    _builder.append(_enumToString_1, "\t");
    _builder.append("</td>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<td>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
    final Consumer<StudyProgram> _function = (StudyProgram it) -> {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _code = it.getCode();
      _builder_1.append(_code);
      _builder_1.append(", ");
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder_1);
    };
    entry.getStudyPrograms().forEach(_function);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("</td>");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("<td>");
    String _name = entry.getRoom().getName();
    _builder_1.append(_name, "\t");
    _builder_1.append("</td>");
    _builder_1.newLineIfNotEmpty();
    _builder_1.append("</tr>");
    _builder_1.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder_1);
  }
  
  public void generateEvaluationForm(final EList<EvaluationFormEntry> evaluationForm, final StringBuilder builder) {
    this.generatePreTable(builder, "Activity", "Weighting");
    final Consumer<EvaluationFormEntry> _function = (EvaluationFormEntry it) -> {
      this.generateTr(it, builder);
    };
    evaluationForm.forEach(_function);
    this.generatePostTable(builder);
  }
  
  public void generateTimetable(final EList<TimetableEntry> timetable, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<div>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<h4>Timetable</h4>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
    boolean _isEmpty = timetable.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      this.generatePreTable(builder, "Day", "Time", "Type", "Planned for", "Room");
      final Consumer<TimetableEntry> _function = (TimetableEntry it) -> {
        this.generateTr(it, builder);
      };
      timetable.forEach(_function);
      this.generatePostTable(builder);
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("<p>There is no timetable available.</p>");
      _builder_1.newLine();
      Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder_1);
    }
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, "</div>");
  }
  
  public void generateCreditReductionsTable(final EList<CreditReduction> creditReductions, final StringBuilder builder) {
    this.generatePreTable(builder, "Course code", "Reduction");
    final Consumer<CreditReduction> _function = (CreditReduction it) -> {
      this.generateTr(it, builder);
    };
    creditReductions.forEach(_function);
    this.generatePostTable(builder);
  }
  
  public void generateStyle(final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<style type=\"text/css\">");
    _builder.newLine();
    _builder.append("* {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("font-family: arial, sans-serif;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("h2, h3 {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("margin-bottom: 20px;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("</style>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generateScript(final Course course, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<script type=\"text/javascript\">");
    _builder.newLine();
    _builder.append("function toggleSelection(code, select) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("var divs;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("divs = document.getElementsByClassName(code + \'Instance\');");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for (var i = 0; i < divs.length; i++) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("divs[i].style.display = \'none\';");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("document.getElementById(select.value).style.display = \'block\';");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("window.onload = function() {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("toggleSelection(\'");
    String _code = course.getCode();
    _builder.append(_code, "\t");
    _builder.append("\', document.getElementById(\'");
    String _code_1 = course.getCode();
    _builder.append(_code_1, "\t");
    _builder.append("InstanceSelect\'));");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.append("</script>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  protected void _generateScript(final EList<Course> courses, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<script type=\"text/javascript\">");
    _builder.newLine();
    _builder.append("function toggleSelection(code, select) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("var divs;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("divs = document.getElementsByClassName(code + \'Instance\');");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for (var i = 0; i < divs.length; i++) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("divs[i].style.display = \'none\';");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("document.getElementById(select.value).style.display = \'block\';");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("window.onload = function() {");
    _builder.newLine();
    {
      for(final Course course : courses) {
        _builder.append("\t");
        _builder.append("toggleSelection(\'");
        String _code = course.getCode();
        _builder.append(_code, "\t");
        _builder.append("\', document.getElementById(\'");
        String _code_1 = course.getCode();
        _builder.append(_code_1, "\t");
        _builder.append("InstanceSelect\'));");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    _builder.append("</script>");
    _builder.newLine();
    Coursesweb2HtmlGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public static String enumToString(final Enumerator eenum) {
    String _xblockexpression = null;
    {
      String enumString = eenum.toString();
      enumString = enumString.toLowerCase();
      enumString = StringExtensions.toFirstUpper(enumString);
      enumString = enumString.replaceAll("_", " ");
      _xblockexpression = enumString;
    }
    return _xblockexpression;
  }
  
  public static StringBuilder operator_doubleLessThan(final StringBuilder stringBuilder, final Object o) {
    return stringBuilder.append(o);
  }
  
  public void generate(final Object course, final StringBuilder builder) {
    if (course instanceof Course) {
      _generate((Course)course, builder);
      return;
    } else if (course instanceof CourseInstance) {
      _generate((CourseInstance)course, builder);
      return;
    } else if (course instanceof CourseWork) {
      _generate((CourseWork)course, builder);
      return;
    } else if (course instanceof Department) {
      _generate((Department)course, builder);
      return;
    } else if (course instanceof EStaffRole) {
      _generate((EStaffRole)course, builder);
      return;
    } else if (course instanceof Person) {
      _generate((Person)course, builder);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(course, builder).toString());
    }
  }
  
  public void generateTr(final EObject course, final StringBuilder builder) {
    if (course instanceof Course) {
      _generateTr((Course)course, builder);
      return;
    } else if (course instanceof CreditReduction) {
      _generateTr((CreditReduction)course, builder);
      return;
    } else if (course instanceof EvaluationFormEntry) {
      _generateTr((EvaluationFormEntry)course, builder);
      return;
    } else if (course instanceof TimetableEntry) {
      _generateTr((TimetableEntry)course, builder);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(course, builder).toString());
    }
  }
  
  public void generateScript(final Object courses, final StringBuilder builder) {
    if (courses instanceof EList) {
      _generateScript((EList<Course>)courses, builder);
      return;
    } else if (courses instanceof Course) {
      _generateScript((Course)courses, builder);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(courses, builder).toString());
    }
  }
}
