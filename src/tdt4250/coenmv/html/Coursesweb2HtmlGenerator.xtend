package tdt4250.coenmv.html

import tdt4250.coenmv.coursesweb.University
import tdt4250.coenmv.coursesweb.Course
import tdt4250.coenmv.coursesweb.CourseInstance
import org.eclipse.emf.common.util.EList
import tdt4250.coenmv.coursesweb.CreditReduction
import tdt4250.coenmv.coursesweb.CourseWork
import tdt4250.coenmv.coursesweb.EvaluationFormEntry
import tdt4250.coenmv.coursesweb.TimetableEntry
import tdt4250.coenmv.coursesweb.Department
import tdt4250.coenmv.coursesweb.Staff
import tdt4250.coenmv.coursesweb.Person
import tdt4250.coenmv.coursesweb.EStaffRole
import org.eclipse.emf.common.util.Enumerator

class Coursesweb2HtmlGenerator {
		
	def String generateHtml(University university) {
		generateHtml(university, new StringBuilder)
	}
	
	def String generateHtml(University university, StringBuilder builder) {
		generatePreHtml(university.name, builder)
		for (i : 0 ..< university.courses.size) {
			if (i > 0) builder << "<br/><hr/><br/>"
			generate(university.courses.get(i), builder)
		}
		generatePostHtml(builder)
		generateScript(university.courses, builder)
		generateStyle(builder)
		builder.toString
	}
	
	def String generateHtml(Course course) {
		generateHtml(course, new StringBuilder)
	}
	
	def String generateHtml(Course course, StringBuilder builder) {
		generatePreHtml(course.name, builder)
		generate(course, builder)
		generatePostHtml(builder)
		generateScript(course, builder)
		generateStyle(builder)
		builder.toString
	}
	
	def void generatePreHtml(String title, StringBuilder builder) {
		builder << 
		'''
		<html>
		<head>
			<title>�title�</title>
			<meta http-equiv="content-type" content="text/html; charset="UTF-8"/>
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		</head>
		<body>
		<div class="container-fluid">
		'''
	}
	
	def void generatePostHtml(StringBuilder builder) {
		builder <<
		'''
		</div>
		</body>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		</html>
		'''
	}
	
	def dispatch void generate(Course course, StringBuilder builder) {
		builder <<
		'''
		<div>
		<h2>�course.code� - �course.name�</h2>
		<div><h4>Content</h4><p>�course.content�</p></div>
		'''
		if (!course.recommendedCourses.isEmpty) {
			builder << "<div><h4>Recommended courses</h4>"
			generateCourseTable(course.recommendedCourses, builder)
			builder << "</div>"
		}
		if (!course.requiredCourses.isEmpty) {
			builder << "<div><h4>Required courses</h4>"
			generateCourseTable(course.requiredCourses, builder)
			builder << "</div>"
		}
		if (!course.creditReductions.isEmpty) {
			builder << "<div><h4>Credit reductions</h4>"
			generateCreditReductionsTable(course.creditReductions, builder)
			builder << "</div>"
		}
		if (!course.evaluationForm.isEmpty) {
			builder << "<div><h4>Evaluation Form</h4>"
			generateEvaluationForm(course.evaluationForm, builder)
			builder << "</div>"
		}
		generateFacts(course, builder)
		builder << "</div>"
		generateInstanceSelect(course, builder)
		course.courseInstances.forEach[generate(it, builder)]
	}
	
	def void generateFacts(Course course, StringBuilder builder) {
		generatePreCard(builder, "Facts")
		generateCardSubtitle(builder, "Credits")
		builder << '''<p class="card-text">�course.credits� SP</p>'''
		generate(course.courseWork, builder)
		generatePostCard(builder)
	}
	
	def void generateInstanceSelect(Course course, StringBuilder builder) {
		val instances = course.courseInstances
		builder << '''<select class="custom-select float-right" id="�course.code�InstanceSelect" onchange="toggleSelection('�course.code�', this)" style="width: 12rem;">'''
		for (instance : instances) {
			val id = '''�instance.course.code�Instance�instance.semester��instance.year�'''
			builder <<
			'''
			<option value="�id�">�instance.semester.enumToString� �instance.year�</option>
			'''
		}
		builder << "</select>"
	}
		
	def dispatch void generate(CourseInstance instance, StringBuilder builder) {
		builder <<
		'''
		<div class="�instance.course.code�Instance" id="�instance.course.code�Instance�instance.semester��instance.year�">
			<h3>Semester data - �instance.semester.enumToString� �instance.year�</h3>
		'''
		generateTimetable(instance.timetable, builder)
		generatePreCard(builder, "Contact information")
		generateStaff(instance.staff, builder)
		builder << "<br/>"
		generate(instance.responsibleDepartment, builder)
		generatePostCard(builder)
		builder << "</div>"
	}
	
	def dispatch void generate(Department department, StringBuilder builder) {
		builder << "<div>"
		generateCardSubtitle(builder, "Responsible department")
		builder <<
		'''
		<p>�department.code� - �department.name�</p>
		'''
		builder << "</div>"
	}
		
	def void generateStaff(EList<Staff> staff, StringBuilder builder) {
		builder << "<div>"
		generateCardSubtitle(builder, "Staff")
		val grouped = staff.groupBy[role]
		for (entry : grouped.entrySet) {
			val role = entry.key as EStaffRole
			val employees = entry.value
			builder << "<div><p class=\"card-text mb-2\">"
			generate(role, builder)
			for (employee : employees) {
				builder << "<br/>"
				generate(employee.person, builder)
			}
			builder << "</p></div>"
		}
		builder << "</div>"
	}

	def void generatePreCard(StringBuilder builder, String title) {
		builder <<
		'''
		<div class="card mb-4" style="display: inline-block;">
			<div class="card-body">
				<h4 class="card-title">�title�</h4>
		'''
	}
	
	def void generateCardSubtitle(StringBuilder builder, String subtitle) {
		builder <<
		'''
		<h6 class="card-subtitle mb-2 text-muted">�subtitle�</h6>
		'''
	}
	
	def void generatePostCard(StringBuilder builder) {
		builder <<
		'''
			</div>
		</div>
		'''
	}

	def void generatePreTable(StringBuilder builder, String... headers) {
		builder <<
		'''
		<table class="table table-striped table-responsive mb-4 mt-0">
			<thead>
				<tr>
		'''
		headers.forEach[builder << '''<th>�it�</th>''']
		builder <<
		'''
				</tr>
			</thead>
			<tbody>
		'''
	}
	
	def void generatePostTable(StringBuilder builder) {
		builder <<
		'''
			</tbody>
		</table>
		'''
	}
	
	def dispatch void generate(CourseWork courseWork, StringBuilder builder) {
		generateCardSubtitle(builder, "Coursework")
		builder <<
		'''
		<p class="card-text">
			Lecture hours: �courseWork.lectureHours�
			<br/>
			Lab hours: �courseWork.labHours�
		</p>
		'''
	}
	
	def void generateCourseTable(EList<Course> courses, StringBuilder builder) {
		generatePreTable(builder, "Course code", "Course name")
		courses.forEach[generateTr(it, builder)]
		generatePostTable(builder)
	}
	
	def dispatch void generate(Person person, StringBuilder builder) {
		builder <<
		'''
		�person.firstName� �person.lastName�
		'''
	}
	
	def dispatch void generate(EStaffRole role, StringBuilder builder) {
		builder <<
		'''
		<i>�role.enumToString�:</i>
		'''
	}
		
	def dispatch void generateTr(Course course, StringBuilder builder) {
		builder <<
		'''
		<tr>
			<td>�course.code�</td>
			<td>�course.name�</td>
		</tr>
		'''
	}
	
	def dispatch void generateTr(CreditReduction creditReduction, StringBuilder builder) {
		builder <<
		'''
		<tr>
			<td>�creditReduction.byCourse.code�</td>
			<td>�creditReduction.reduction�</td>
		</tr>
		'''
	}
	
	def dispatch void generateTr(EvaluationFormEntry entry, StringBuilder builder) {
		builder <<
		'''
		<tr>
			<td>�entry.type�</td>
			<td>�entry.weighting�/100</td>
		</tr>
		'''
	}
	
	def dispatch void generateTr(TimetableEntry entry, StringBuilder builder) {
		builder <<
		'''
		<tr>
			<td>�entry.weekDay.enumToString�</td>
			<td>�entry.timeslot�</td>
			<td>�entry.classType.enumToString�</td>
			<td>
		'''
		entry.studyPrograms.forEach[builder << '''�it.code�, ''']
		builder <<
		'''
		</td>
			<td>�entry.room.name�</td>
		</tr>
		'''
	}
	
	def void generateEvaluationForm(EList<EvaluationFormEntry> evaluationForm, StringBuilder builder) {
		generatePreTable(builder, "Activity", "Weighting")
		evaluationForm.forEach[generateTr(it, builder)]
		generatePostTable(builder)
	}
	
	def void generateTimetable(EList<TimetableEntry> timetable, StringBuilder builder) {
		builder <<
		'''
		<div>
			<h4>Timetable</h4>
		'''
		if (!timetable.isEmpty) {
			generatePreTable(builder, "Day", "Time", "Type", "Planned for", "Room")
			timetable.forEach[generateTr(it, builder)]
			generatePostTable(builder)
		} else {
			builder <<
			'''
			<p>There is no timetable available.</p>
			'''
		}
		builder << "</div>"
	}
	
	def void generateCreditReductionsTable(EList<CreditReduction> creditReductions, StringBuilder builder) {
		generatePreTable(builder, "Course code", "Reduction")
		creditReductions.forEach[generateTr(it, builder)]
		generatePostTable(builder)
	}
	
	def void generateStyle(StringBuilder builder) {
		builder <<
		'''
		<style type="text/css">
		* {
			font-family: arial, sans-serif;
		}
		h2, h3 {
			margin-bottom: 20px;
		}
		</style>
		'''
	}
	
	def dispatch void generateScript(Course course, StringBuilder builder) {
		builder <<
		'''
		<script type="text/javascript">
		function toggleSelection(code, select) {
			var divs;
			divs = document.getElementsByClassName(code + 'Instance');
			for (var i = 0; i < divs.length; i++) {
				divs[i].style.display = 'none';
			}
			document.getElementById(select.value).style.display = 'block';
		}
		window.onload = function() {
			toggleSelection('�course.code�', document.getElementById('�course.code�InstanceSelect'));
		}
		</script>
		'''
	}
	
	def dispatch void generateScript(EList<Course> courses, StringBuilder builder) {
		builder <<
		'''
		<script type="text/javascript">
		function toggleSelection(code, select) {
			var divs;
			divs = document.getElementsByClassName(code + 'Instance');
			for (var i = 0; i < divs.length; i++) {
				divs[i].style.display = 'none';
			}
			document.getElementById(select.value).style.display = 'block';
		}
		window.onload = function() {
			�FOR course : courses�
				toggleSelection('�course.code�', document.getElementById('�course.code�InstanceSelect'));
			�ENDFOR�
		}
		</script>
		'''
	}
	
	def static String enumToString(Enumerator eenum) {
		var enumString = eenum.toString
		enumString = enumString.toLowerCase
		enumString = enumString.toFirstUpper
		enumString = enumString.replaceAll("_", " ")
		enumString
	}

	// << operator
	def static StringBuilder operator_doubleLessThan(StringBuilder stringBuilder, Object o) {
		return stringBuilder.append(o);
	}
	
}