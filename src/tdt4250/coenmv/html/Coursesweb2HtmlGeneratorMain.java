package tdt4250.coenmv.html;

import java.io.IOException;
import java.io.PrintStream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import tdt4250.coenmv.coursesweb.Course;
import tdt4250.coenmv.coursesweb.CourseInstance;
import tdt4250.coenmv.coursesweb.CourseswebFactory;
import tdt4250.coenmv.coursesweb.CourseswebPackage;
import tdt4250.coenmv.coursesweb.TimetableEntry;
import tdt4250.coenmv.coursesweb.University;
import tdt4250.coenmv.coursesweb.util.CourseswebResourceFactoryImpl;
import tdt4250.coenmv.coursesweb.util.Timeslot;

public class Coursesweb2HtmlGeneratorMain {
	
	public static void main(String[] args) {
		try {
			University university = (args.length > 0 ? getUniversity(args[0]) : getSampleUniversity());
			setSampleTimeslots(university);
			if (args.length == 2 + university.getCourses().size()) {
				for (int i = 0; i < university.getCourses().size(); i++) {
					String html = new Coursesweb2HtmlGenerator().generateHtml(university.getCourses().get(i));
					URI target = URI.createURI(args[1] + "/" + args[2 + i]);
					PrintStream ps = new PrintStream(university.eResource().getResourceSet().getURIConverter().createOutputStream(target));
					ps.print(html);
				}
			} else {
				String html = new Coursesweb2HtmlGenerator().generateHtml(university);
				if (args.length == 2) {
					URI target = URI.createURI(args[1]);
					PrintStream ps = new PrintStream(university.eResource().getResourceSet().getURIConverter().createOutputStream(target));
					ps.print(html);
				} else {
					System.out.println(html);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void setSampleTimeslots(University university) {
		EList<Course> courses = university.getCourses();
		for (Course course : courses) {
			EList<CourseInstance> courseInstances = course.getCourseInstances();
			for (CourseInstance instance : courseInstances) {
				EList<TimetableEntry> timetable = instance.getTimetable();
				EDataType timeslotType = CourseswebPackage.eINSTANCE.getETimeslot();
				if (timetable.size() > 0) {
					Timeslot timeslot = (Timeslot) CourseswebFactory.eINSTANCE.createFromString(timeslotType, "10:15-11:00");
					setTimeslot(timetable.get(0), timeslot);
					timeslot = (Timeslot) CourseswebFactory.eINSTANCE.createFromString(timeslotType, "14:15-16:00");
					setTimeslot(timetable.get(1), timeslot);
					timeslot = (Timeslot) CourseswebFactory.eINSTANCE.createFromString(timeslotType, "10:15-12:00");
					setTimeslot(timetable.get(2), timeslot);
					timeslot = (Timeslot) CourseswebFactory.eINSTANCE.createFromString(timeslotType, "12:15-14:00");
					setTimeslot(timetable.get(3), timeslot);
				}
			}
		}
	}
		
	public static void setTimeslot(TimetableEntry timetableEntry, Timeslot timeslot) {
		if (timetableEntry != null) {
			timetableEntry.setTimeslot(timeslot);
		}
	}
	
	public static University getUniversity(String uriString) throws IOException {
		ResourceSet resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(CourseswebPackage.eNS_URI, CourseswebPackage.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new CourseswebResourceFactoryImpl());
		Resource resource = resSet.getResource(URI.createURI(uriString), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof University) {
				return (University) eObject;
			}
		}
		return null;
	}
	
	public static University getSampleUniversity() {
		try {
			return getUniversity(Coursesweb2HtmlGenerator.class.getResource("NTNU.xmi").toString());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
