/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.University#getCourses <em>Courses</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.University#getDepartments <em>Departments</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.University#getPersons <em>Persons</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.University#getRooms <em>Rooms</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.University#getStudyPrograms <em>Study Programs</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.University#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getUniversity()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='uniqueCourseCodes uniqueDepartmentCodes uniqueStudyProgramCodes'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL uniqueCourseCodes='self.courses-&gt;forAll(c1, c2 | c1 &lt;&gt; c2 implies c1.code &lt;&gt; c2.code)' uniqueDepartmentCodes='self.departments-&gt;forAll(d1, d2 | d1 &lt;&gt; d2 implies d1.code &lt;&gt; d2.code)' uniqueStudyProgramCodes='self.studyPrograms-&gt;forAll(sp1, sp2 | sp1 &lt;&gt; sp2 implies sp1.code &lt;&gt; sp2.code)'"
 * @generated
 */
public interface University extends EObject {
	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getUniversity_Courses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Departments</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.Department}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Departments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Departments</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getUniversity_Departments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Department> getDepartments();

	/**
	 * Returns the value of the '<em><b>Persons</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persons</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persons</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getUniversity_Persons()
	 * @model containment="true"
	 * @generated
	 */
	EList<Person> getPersons();

	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getUniversity_Rooms()
	 * @model containment="true"
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * Returns the value of the '<em><b>Study Programs</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Study Programs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Programs</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getUniversity_StudyPrograms()
	 * @model containment="true"
	 * @generated
	 */
	EList<StudyProgram> getStudyPrograms();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getUniversity_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.University#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // University
