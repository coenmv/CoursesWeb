/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Credit Reduction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.CreditReduction#getReduction <em>Reduction</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.CreditReduction#getByCourse <em>By Course</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.CreditReduction#getOnCourse <em>On Course</em>}</li>
 * </ul>
 *
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCreditReduction()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='onCourseOtherThanByCourse reductionWithinRange'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL onCourseOtherThanByCourse='self.onCourse &lt;&gt; self.byCourse' reductionWithinRange='self.reduction &gt;= 0.0 and self.reduction &lt;= self.onCourse.credits'"
 * @generated
 */
public interface CreditReduction extends EObject {
	/**
	 * Returns the value of the '<em><b>Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reduction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reduction</em>' attribute.
	 * @see #setReduction(float)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCreditReduction_Reduction()
	 * @model
	 * @generated
	 */
	float getReduction();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.CreditReduction#getReduction <em>Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reduction</em>' attribute.
	 * @see #getReduction()
	 * @generated
	 */
	void setReduction(float value);

	/**
	 * Returns the value of the '<em><b>By Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>By Course</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>By Course</em>' reference.
	 * @see #setByCourse(Course)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCreditReduction_ByCourse()
	 * @model
	 * @generated
	 */
	Course getByCourse();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.CreditReduction#getByCourse <em>By Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>By Course</em>' reference.
	 * @see #getByCourse()
	 * @generated
	 */
	void setByCourse(Course value);

	/**
	 * Returns the value of the '<em><b>On Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.coenmv.coursesweb.Course#getCreditReductions <em>Credit Reductions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Course</em>' container reference.
	 * @see #setOnCourse(Course)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCreditReduction_OnCourse()
	 * @see tdt4250.coenmv.coursesweb.Course#getCreditReductions
	 * @model opposite="creditReductions" transient="false"
	 * @generated
	 */
	Course getOnCourse();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.CreditReduction#getOnCourse <em>On Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Course</em>' container reference.
	 * @see #getOnCourse()
	 * @generated
	 */
	void setOnCourse(Course value);

} // CreditReduction
