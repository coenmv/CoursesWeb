/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation Form Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.EvaluationFormEntry#getWeighting <em>Weighting</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.EvaluationFormEntry#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getEvaluationFormEntry()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='weightingWithinRange'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL weightingWithinRange='self.weighting &gt;= 0 and self.weighting &lt;= 100'"
 * @generated
 */
public interface EvaluationFormEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Weighting</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Weighting</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weighting</em>' attribute.
	 * @see #setWeighting(int)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getEvaluationFormEntry_Weighting()
	 * @model default="0"
	 * @generated
	 */
	int getWeighting();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.EvaluationFormEntry#getWeighting <em>Weighting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weighting</em>' attribute.
	 * @see #getWeighting()
	 * @generated
	 */
	void setWeighting(int value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getEvaluationFormEntry_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.EvaluationFormEntry#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // EvaluationFormEntry
