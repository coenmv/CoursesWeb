/**
 */
package tdt4250.coenmv.coursesweb.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import tdt4250.coenmv.coursesweb.*;

import tdt4250.coenmv.coursesweb.util.Timeslot;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CourseswebFactoryImpl extends EFactoryImpl implements CourseswebFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CourseswebFactory init() {
		try {
			CourseswebFactory theCourseswebFactory = (CourseswebFactory)EPackage.Registry.INSTANCE.getEFactory(CourseswebPackage.eNS_URI);
			if (theCourseswebFactory != null) {
				return theCourseswebFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CourseswebFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseswebFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CourseswebPackage.UNIVERSITY: return createUniversity();
			case CourseswebPackage.COURSE: return createCourse();
			case CourseswebPackage.COURSE_INSTANCE: return createCourseInstance();
			case CourseswebPackage.STAFF: return createStaff();
			case CourseswebPackage.TIMETABLE_ENTRY: return createTimetableEntry();
			case CourseswebPackage.EVALUATION_FORM_ENTRY: return createEvaluationFormEntry();
			case CourseswebPackage.CREDIT_REDUCTION: return createCreditReduction();
			case CourseswebPackage.COURSE_WORK: return createCourseWork();
			case CourseswebPackage.DEPARTMENT: return createDepartment();
			case CourseswebPackage.PERSON: return createPerson();
			case CourseswebPackage.ROOM: return createRoom();
			case CourseswebPackage.STUDY_PROGRAM: return createStudyProgram();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case CourseswebPackage.ESTAFF_ROLE:
				return createEStaffRoleFromString(eDataType, initialValue);
			case CourseswebPackage.ESEMESTER:
				return createESemesterFromString(eDataType, initialValue);
			case CourseswebPackage.EWEEK_DAY:
				return createEWeekDayFromString(eDataType, initialValue);
			case CourseswebPackage.ECLASS_TYPE:
				return createEClassTypeFromString(eDataType, initialValue);
			case CourseswebPackage.ETIMESLOT:
				return createETimeslotFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case CourseswebPackage.ESTAFF_ROLE:
				return convertEStaffRoleToString(eDataType, instanceValue);
			case CourseswebPackage.ESEMESTER:
				return convertESemesterToString(eDataType, instanceValue);
			case CourseswebPackage.EWEEK_DAY:
				return convertEWeekDayToString(eDataType, instanceValue);
			case CourseswebPackage.ECLASS_TYPE:
				return convertEClassTypeToString(eDataType, instanceValue);
			case CourseswebPackage.ETIMESLOT:
				return convertETimeslotToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public University createUniversity() {
		UniversityImpl university = new UniversityImpl();
		return university;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course createCourse() {
		CourseImpl course = new CourseImpl();
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance createCourseInstance() {
		CourseInstanceImpl courseInstance = new CourseInstanceImpl();
		return courseInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Staff createStaff() {
		StaffImpl staff = new StaffImpl();
		return staff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department createDepartment() {
		DepartmentImpl department = new DepartmentImpl();
		return department;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimetableEntry createTimetableEntry() {
		TimetableEntryImpl timetableEntry = new TimetableEntryImpl();
		return timetableEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationFormEntry createEvaluationFormEntry() {
		EvaluationFormEntryImpl evaluationFormEntry = new EvaluationFormEntryImpl();
		return evaluationFormEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreditReduction createCreditReduction() {
		CreditReductionImpl creditReduction = new CreditReductionImpl();
		return creditReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork createCourseWork() {
		CourseWorkImpl courseWork = new CourseWorkImpl();
		return courseWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person createPerson() {
		PersonImpl person = new PersonImpl();
		return person;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room createRoom() {
		RoomImpl room = new RoomImpl();
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgram createStudyProgram() {
		StudyProgramImpl studyProgram = new StudyProgramImpl();
		return studyProgram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStaffRole createEStaffRoleFromString(EDataType eDataType, String initialValue) {
		EStaffRole result = EStaffRole.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEStaffRoleToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ESemester createESemesterFromString(EDataType eDataType, String initialValue) {
		ESemester result = ESemester.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertESemesterToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EWeekDay createEWeekDayFromString(EDataType eDataType, String initialValue) {
		EWeekDay result = EWeekDay.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEWeekDayToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassType createEClassTypeFromString(EDataType eDataType, String initialValue) {
		EClassType result = EClassType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEClassTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Timeslot createETimeslotFromString(EDataType eDataType, String initialValue) {
		int pos = initialValue.indexOf('-');
		if (pos < 0) {
			return null;
		}
		String startTimeString = initialValue.substring(0, pos);
		String endTimeString = initialValue.substring(pos + 1);
		int startSplitPos = startTimeString.indexOf(':');
		int endSplitPos = endTimeString.indexOf(':');
		if (startSplitPos < 0 || endSplitPos < 0) {
			return null;
		}
		String startHoursString = startTimeString.substring(0, startSplitPos);
		String startMinutesString = startTimeString.substring(startSplitPos + 1);
		String endHoursString = endTimeString.substring(0, endSplitPos);
		String endMinutesString = endTimeString.substring(endSplitPos + 1);
		try {
			return new Timeslot(Integer.parseInt(startHoursString), Integer.parseInt(startMinutesString), Integer.parseInt(endHoursString), Integer.parseInt(endMinutesString));
		} catch (NumberFormatException e) {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertETimeslotToString(EDataType eDataType, Object instanceValue) {
		if (instanceValue == null) {
			return "";
		}
		Timeslot timeslot = (Timeslot) instanceValue;
		return timeslot.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseswebPackage getCourseswebPackage() {
		return (CourseswebPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CourseswebPackage getPackage() {
		return CourseswebPackage.eINSTANCE;
	}

} //CourseswebFactoryImpl
