/**
 */
package tdt4250.coenmv.coursesweb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.coenmv.coursesweb.Course;
import tdt4250.coenmv.coursesweb.CourseswebPackage;
import tdt4250.coenmv.coursesweb.Department;
import tdt4250.coenmv.coursesweb.Person;
import tdt4250.coenmv.coursesweb.Room;
import tdt4250.coenmv.coursesweb.StudyProgram;
import tdt4250.coenmv.coursesweb.University;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.UniversityImpl#getCourses <em>Courses</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.UniversityImpl#getDepartments <em>Departments</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.UniversityImpl#getPersons <em>Persons</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.UniversityImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.UniversityImpl#getStudyPrograms <em>Study Programs</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.UniversityImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversityImpl extends MinimalEObjectImpl.Container implements University {
	/**
	 * The cached value of the '{@link #getCourses() <em>Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> courses;

	/**
	 * The cached value of the '{@link #getDepartments() <em>Departments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepartments()
	 * @generated
	 * @ordered
	 */
	protected EList<Department> departments;

	/**
	 * The cached value of the '{@link #getPersons() <em>Persons</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersons()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> persons;

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getStudyPrograms() <em>Study Programs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyPrograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyPrograms;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UniversityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseswebPackage.Literals.UNIVERSITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourses() {
		if (courses == null) {
			courses = new EObjectContainmentEList<Course>(Course.class, this, CourseswebPackage.UNIVERSITY__COURSES);
		}
		return courses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Department> getDepartments() {
		if (departments == null) {
			departments = new EObjectContainmentEList<Department>(Department.class, this, CourseswebPackage.UNIVERSITY__DEPARTMENTS);
		}
		return departments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getPersons() {
		if (persons == null) {
			persons = new EObjectContainmentEList<Person>(Person.class, this, CourseswebPackage.UNIVERSITY__PERSONS);
		}
		return persons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectContainmentEList<Room>(Room.class, this, CourseswebPackage.UNIVERSITY__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getStudyPrograms() {
		if (studyPrograms == null) {
			studyPrograms = new EObjectContainmentEList<StudyProgram>(StudyProgram.class, this, CourseswebPackage.UNIVERSITY__STUDY_PROGRAMS);
		}
		return studyPrograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.UNIVERSITY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CourseswebPackage.UNIVERSITY__COURSES:
				return ((InternalEList<?>)getCourses()).basicRemove(otherEnd, msgs);
			case CourseswebPackage.UNIVERSITY__DEPARTMENTS:
				return ((InternalEList<?>)getDepartments()).basicRemove(otherEnd, msgs);
			case CourseswebPackage.UNIVERSITY__PERSONS:
				return ((InternalEList<?>)getPersons()).basicRemove(otherEnd, msgs);
			case CourseswebPackage.UNIVERSITY__ROOMS:
				return ((InternalEList<?>)getRooms()).basicRemove(otherEnd, msgs);
			case CourseswebPackage.UNIVERSITY__STUDY_PROGRAMS:
				return ((InternalEList<?>)getStudyPrograms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CourseswebPackage.UNIVERSITY__COURSES:
				return getCourses();
			case CourseswebPackage.UNIVERSITY__DEPARTMENTS:
				return getDepartments();
			case CourseswebPackage.UNIVERSITY__PERSONS:
				return getPersons();
			case CourseswebPackage.UNIVERSITY__ROOMS:
				return getRooms();
			case CourseswebPackage.UNIVERSITY__STUDY_PROGRAMS:
				return getStudyPrograms();
			case CourseswebPackage.UNIVERSITY__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CourseswebPackage.UNIVERSITY__COURSES:
				getCourses().clear();
				getCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case CourseswebPackage.UNIVERSITY__DEPARTMENTS:
				getDepartments().clear();
				getDepartments().addAll((Collection<? extends Department>)newValue);
				return;
			case CourseswebPackage.UNIVERSITY__PERSONS:
				getPersons().clear();
				getPersons().addAll((Collection<? extends Person>)newValue);
				return;
			case CourseswebPackage.UNIVERSITY__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case CourseswebPackage.UNIVERSITY__STUDY_PROGRAMS:
				getStudyPrograms().clear();
				getStudyPrograms().addAll((Collection<? extends StudyProgram>)newValue);
				return;
			case CourseswebPackage.UNIVERSITY__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CourseswebPackage.UNIVERSITY__COURSES:
				getCourses().clear();
				return;
			case CourseswebPackage.UNIVERSITY__DEPARTMENTS:
				getDepartments().clear();
				return;
			case CourseswebPackage.UNIVERSITY__PERSONS:
				getPersons().clear();
				return;
			case CourseswebPackage.UNIVERSITY__ROOMS:
				getRooms().clear();
				return;
			case CourseswebPackage.UNIVERSITY__STUDY_PROGRAMS:
				getStudyPrograms().clear();
				return;
			case CourseswebPackage.UNIVERSITY__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CourseswebPackage.UNIVERSITY__COURSES:
				return courses != null && !courses.isEmpty();
			case CourseswebPackage.UNIVERSITY__DEPARTMENTS:
				return departments != null && !departments.isEmpty();
			case CourseswebPackage.UNIVERSITY__PERSONS:
				return persons != null && !persons.isEmpty();
			case CourseswebPackage.UNIVERSITY__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case CourseswebPackage.UNIVERSITY__STUDY_PROGRAMS:
				return studyPrograms != null && !studyPrograms.isEmpty();
			case CourseswebPackage.UNIVERSITY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //UniversityImpl
