/**
 */
package tdt4250.coenmv.coursesweb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import tdt4250.coenmv.coursesweb.Course;
import tdt4250.coenmv.coursesweb.CourseInstance;
import tdt4250.coenmv.coursesweb.CourseswebPackage;
import tdt4250.coenmv.coursesweb.Department;
import tdt4250.coenmv.coursesweb.ESemester;
import tdt4250.coenmv.coursesweb.Staff;
import tdt4250.coenmv.coursesweb.TimetableEntry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl#getResponsibleDepartment <em>Responsible Department</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl#getStaff <em>Staff</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl#getSemester <em>Semester</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInstanceImpl extends MinimalEObjectImpl.Container implements CourseInstance {
	/**
	 * The cached value of the '{@link #getTimetable() <em>Timetable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimetable()
	 * @generated
	 * @ordered
	 */
	protected EList<TimetableEntry> timetable;

	/**
	 * The cached value of the '{@link #getResponsibleDepartment() <em>Responsible Department</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleDepartment()
	 * @generated
	 * @ordered
	 */
	protected Department responsibleDepartment;

	/**
	 * The cached value of the '{@link #getStaff() <em>Staff</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaff()
	 * @generated
	 * @ordered
	 */
	protected EList<Staff> staff;

	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected static final ESemester SEMESTER_EDEFAULT = ESemester.AUTUMN;

	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected ESemester semester = SEMESTER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseswebPackage.Literals.COURSE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TimetableEntry> getTimetable() {
		if (timetable == null) {
			timetable = new EObjectContainmentEList<TimetableEntry>(TimetableEntry.class, this, CourseswebPackage.COURSE_INSTANCE__TIMETABLE);
		}
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getResponsibleDepartment() {
		if (responsibleDepartment != null && responsibleDepartment.eIsProxy()) {
			InternalEObject oldResponsibleDepartment = (InternalEObject)responsibleDepartment;
			responsibleDepartment = (Department)eResolveProxy(oldResponsibleDepartment);
			if (responsibleDepartment != oldResponsibleDepartment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CourseswebPackage.COURSE_INSTANCE__RESPONSIBLE_DEPARTMENT, oldResponsibleDepartment, responsibleDepartment));
			}
		}
		return responsibleDepartment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department basicGetResponsibleDepartment() {
		return responsibleDepartment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponsibleDepartment(Department newResponsibleDepartment) {
		Department oldResponsibleDepartment = responsibleDepartment;
		responsibleDepartment = newResponsibleDepartment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE_INSTANCE__RESPONSIBLE_DEPARTMENT, oldResponsibleDepartment, responsibleDepartment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Staff> getStaff() {
		if (staff == null) {
			staff = new EObjectContainmentEList<Staff>(Staff.class, this, CourseswebPackage.COURSE_INSTANCE__STAFF);
		}
		return staff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE_INSTANCE__YEAR, oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ESemester getSemester() {
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemester(ESemester newSemester) {
		ESemester oldSemester = semester;
		semester = newSemester == null ? SEMESTER_EDEFAULT : newSemester;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE_INSTANCE__SEMESTER, oldSemester, semester));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCourse() {
		if (eContainerFeatureID() != CourseswebPackage.COURSE_INSTANCE__COURSE) return null;
		return (Course)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(Course newCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCourse, CourseswebPackage.COURSE_INSTANCE__COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(Course newCourse) {
		if (newCourse != eInternalContainer() || (eContainerFeatureID() != CourseswebPackage.COURSE_INSTANCE__COURSE && newCourse != null)) {
			if (EcoreUtil.isAncestor(this, newCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourse != null)
				msgs = ((InternalEObject)newCourse).eInverseAdd(this, CourseswebPackage.COURSE__COURSE_INSTANCES, Course.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE_INSTANCE__COURSE, newCourse, newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CourseswebPackage.COURSE_INSTANCE__COURSE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCourse((Course)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CourseswebPackage.COURSE_INSTANCE__TIMETABLE:
				return ((InternalEList<?>)getTimetable()).basicRemove(otherEnd, msgs);
			case CourseswebPackage.COURSE_INSTANCE__STAFF:
				return ((InternalEList<?>)getStaff()).basicRemove(otherEnd, msgs);
			case CourseswebPackage.COURSE_INSTANCE__COURSE:
				return basicSetCourse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CourseswebPackage.COURSE_INSTANCE__COURSE:
				return eInternalContainer().eInverseRemove(this, CourseswebPackage.COURSE__COURSE_INSTANCES, Course.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CourseswebPackage.COURSE_INSTANCE__TIMETABLE:
				return getTimetable();
			case CourseswebPackage.COURSE_INSTANCE__RESPONSIBLE_DEPARTMENT:
				if (resolve) return getResponsibleDepartment();
				return basicGetResponsibleDepartment();
			case CourseswebPackage.COURSE_INSTANCE__STAFF:
				return getStaff();
			case CourseswebPackage.COURSE_INSTANCE__YEAR:
				return getYear();
			case CourseswebPackage.COURSE_INSTANCE__SEMESTER:
				return getSemester();
			case CourseswebPackage.COURSE_INSTANCE__COURSE:
				return getCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CourseswebPackage.COURSE_INSTANCE__TIMETABLE:
				getTimetable().clear();
				getTimetable().addAll((Collection<? extends TimetableEntry>)newValue);
				return;
			case CourseswebPackage.COURSE_INSTANCE__RESPONSIBLE_DEPARTMENT:
				setResponsibleDepartment((Department)newValue);
				return;
			case CourseswebPackage.COURSE_INSTANCE__STAFF:
				getStaff().clear();
				getStaff().addAll((Collection<? extends Staff>)newValue);
				return;
			case CourseswebPackage.COURSE_INSTANCE__YEAR:
				setYear((Integer)newValue);
				return;
			case CourseswebPackage.COURSE_INSTANCE__SEMESTER:
				setSemester((ESemester)newValue);
				return;
			case CourseswebPackage.COURSE_INSTANCE__COURSE:
				setCourse((Course)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CourseswebPackage.COURSE_INSTANCE__TIMETABLE:
				getTimetable().clear();
				return;
			case CourseswebPackage.COURSE_INSTANCE__RESPONSIBLE_DEPARTMENT:
				setResponsibleDepartment((Department)null);
				return;
			case CourseswebPackage.COURSE_INSTANCE__STAFF:
				getStaff().clear();
				return;
			case CourseswebPackage.COURSE_INSTANCE__YEAR:
				setYear(YEAR_EDEFAULT);
				return;
			case CourseswebPackage.COURSE_INSTANCE__SEMESTER:
				setSemester(SEMESTER_EDEFAULT);
				return;
			case CourseswebPackage.COURSE_INSTANCE__COURSE:
				setCourse((Course)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CourseswebPackage.COURSE_INSTANCE__TIMETABLE:
				return timetable != null && !timetable.isEmpty();
			case CourseswebPackage.COURSE_INSTANCE__RESPONSIBLE_DEPARTMENT:
				return responsibleDepartment != null;
			case CourseswebPackage.COURSE_INSTANCE__STAFF:
				return staff != null && !staff.isEmpty();
			case CourseswebPackage.COURSE_INSTANCE__YEAR:
				return year != YEAR_EDEFAULT;
			case CourseswebPackage.COURSE_INSTANCE__SEMESTER:
				return semester != SEMESTER_EDEFAULT;
			case CourseswebPackage.COURSE_INSTANCE__COURSE:
				return getCourse() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (year: ");
		result.append(year);
		result.append(", semester: ");
		result.append(semester);
		result.append(')');
		return result.toString();
	}

} //CourseInstanceImpl
