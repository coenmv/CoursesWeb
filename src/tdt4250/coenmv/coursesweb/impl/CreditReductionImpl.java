/**
 */
package tdt4250.coenmv.coursesweb.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;
import tdt4250.coenmv.coursesweb.Course;
import tdt4250.coenmv.coursesweb.CourseswebPackage;
import tdt4250.coenmv.coursesweb.CreditReduction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Credit Reduction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CreditReductionImpl#getReduction <em>Reduction</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CreditReductionImpl#getByCourse <em>By Course</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CreditReductionImpl#getOnCourse <em>On Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CreditReductionImpl extends MinimalEObjectImpl.Container implements CreditReduction {
	/**
	 * The default value of the '{@link #getReduction() <em>Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReduction()
	 * @generated
	 * @ordered
	 */
	protected static final float REDUCTION_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getReduction() <em>Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReduction()
	 * @generated
	 * @ordered
	 */
	protected float reduction = REDUCTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getByCourse() <em>By Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getByCourse()
	 * @generated
	 * @ordered
	 */
	protected Course byCourse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CreditReductionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseswebPackage.Literals.CREDIT_REDUCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getReduction() {
		return reduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReduction(float newReduction) {
		float oldReduction = reduction;
		reduction = newReduction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.CREDIT_REDUCTION__REDUCTION, oldReduction, reduction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getByCourse() {
		if (byCourse != null && byCourse.eIsProxy()) {
			InternalEObject oldByCourse = (InternalEObject)byCourse;
			byCourse = (Course)eResolveProxy(oldByCourse);
			if (byCourse != oldByCourse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CourseswebPackage.CREDIT_REDUCTION__BY_COURSE, oldByCourse, byCourse));
			}
		}
		return byCourse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course basicGetByCourse() {
		return byCourse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setByCourse(Course newByCourse) {
		Course oldByCourse = byCourse;
		byCourse = newByCourse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.CREDIT_REDUCTION__BY_COURSE, oldByCourse, byCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getOnCourse() {
		if (eContainerFeatureID() != CourseswebPackage.CREDIT_REDUCTION__ON_COURSE) return null;
		return (Course)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOnCourse(Course newOnCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newOnCourse, CourseswebPackage.CREDIT_REDUCTION__ON_COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnCourse(Course newOnCourse) {
		if (newOnCourse != eInternalContainer() || (eContainerFeatureID() != CourseswebPackage.CREDIT_REDUCTION__ON_COURSE && newOnCourse != null)) {
			if (EcoreUtil.isAncestor(this, newOnCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOnCourse != null)
				msgs = ((InternalEObject)newOnCourse).eInverseAdd(this, CourseswebPackage.COURSE__CREDIT_REDUCTIONS, Course.class, msgs);
			msgs = basicSetOnCourse(newOnCourse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.CREDIT_REDUCTION__ON_COURSE, newOnCourse, newOnCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CourseswebPackage.CREDIT_REDUCTION__ON_COURSE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetOnCourse((Course)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CourseswebPackage.CREDIT_REDUCTION__ON_COURSE:
				return basicSetOnCourse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CourseswebPackage.CREDIT_REDUCTION__ON_COURSE:
				return eInternalContainer().eInverseRemove(this, CourseswebPackage.COURSE__CREDIT_REDUCTIONS, Course.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CourseswebPackage.CREDIT_REDUCTION__REDUCTION:
				return getReduction();
			case CourseswebPackage.CREDIT_REDUCTION__BY_COURSE:
				if (resolve) return getByCourse();
				return basicGetByCourse();
			case CourseswebPackage.CREDIT_REDUCTION__ON_COURSE:
				return getOnCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CourseswebPackage.CREDIT_REDUCTION__REDUCTION:
				setReduction((Float)newValue);
				return;
			case CourseswebPackage.CREDIT_REDUCTION__BY_COURSE:
				setByCourse((Course)newValue);
				return;
			case CourseswebPackage.CREDIT_REDUCTION__ON_COURSE:
				setOnCourse((Course)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CourseswebPackage.CREDIT_REDUCTION__REDUCTION:
				setReduction(REDUCTION_EDEFAULT);
				return;
			case CourseswebPackage.CREDIT_REDUCTION__BY_COURSE:
				setByCourse((Course)null);
				return;
			case CourseswebPackage.CREDIT_REDUCTION__ON_COURSE:
				setOnCourse((Course)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CourseswebPackage.CREDIT_REDUCTION__REDUCTION:
				return reduction != REDUCTION_EDEFAULT;
			case CourseswebPackage.CREDIT_REDUCTION__BY_COURSE:
				return byCourse != null;
			case CourseswebPackage.CREDIT_REDUCTION__ON_COURSE:
				return getOnCourse() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (reduction: ");
		result.append(reduction);
		result.append(')');
		return result.toString();
	}

} //CreditReductionImpl
