/**
 */
package tdt4250.coenmv.coursesweb.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.coenmv.coursesweb.Course;
import tdt4250.coenmv.coursesweb.CourseInstance;
import tdt4250.coenmv.coursesweb.CourseWork;
import tdt4250.coenmv.coursesweb.CourseswebPackage;
import tdt4250.coenmv.coursesweb.CreditReduction;
import tdt4250.coenmv.coursesweb.EvaluationFormEntry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getCourseInstances <em>Course Instances</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getRequiredCourses <em>Required Courses</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getRecommendedCourses <em>Recommended Courses</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getCreditReductions <em>Credit Reductions</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getCourseWork <em>Course Work</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.CourseImpl#getEvaluationForm <em>Evaluation Form</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final float CREDITS_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected float credits = CREDITS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourseInstances() <em>Course Instances</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> courseInstances;

	/**
	 * The cached value of the '{@link #getRequiredCourses() <em>Required Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> requiredCourses;

	/**
	 * The cached value of the '{@link #getRecommendedCourses() <em>Recommended Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecommendedCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> recommendedCourses;

	/**
	 * The cached value of the '{@link #getCreditReductions() <em>Credit Reductions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditReductions()
	 * @generated
	 * @ordered
	 */
	protected EList<CreditReduction> creditReductions;

	/**
	 * The cached value of the '{@link #getCourseWork() <em>Course Work</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseWork()
	 * @generated
	 * @ordered
	 */
	protected CourseWork courseWork;

	/**
	 * The cached value of the '{@link #getEvaluationForm() <em>Evaluation Form</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationForm()
	 * @generated
	 * @ordered
	 */
	protected EList<EvaluationFormEntry> evaluationForm;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseswebPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(float newCredits) {
		float oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE__CREDITS, oldCredits, credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getCourseInstances() {
		if (courseInstances == null) {
			courseInstances = new EObjectContainmentWithInverseEList<CourseInstance>(CourseInstance.class, this, CourseswebPackage.COURSE__COURSE_INSTANCES, CourseswebPackage.COURSE_INSTANCE__COURSE);
		}
		return courseInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRequiredCourses() {
		if (requiredCourses == null) {
			requiredCourses = new EObjectResolvingEList<Course>(Course.class, this, CourseswebPackage.COURSE__REQUIRED_COURSES);
		}
		return requiredCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRecommendedCourses() {
		if (recommendedCourses == null) {
			recommendedCourses = new EObjectResolvingEList<Course>(Course.class, this, CourseswebPackage.COURSE__RECOMMENDED_COURSES);
		}
		return recommendedCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CreditReduction> getCreditReductions() {
		if (creditReductions == null) {
			creditReductions = new EObjectContainmentWithInverseEList<CreditReduction>(CreditReduction.class, this, CourseswebPackage.COURSE__CREDIT_REDUCTIONS, CourseswebPackage.CREDIT_REDUCTION__ON_COURSE);
		}
		return creditReductions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork getCourseWork() {
		return courseWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseWork(CourseWork newCourseWork, NotificationChain msgs) {
		CourseWork oldCourseWork = courseWork;
		courseWork = newCourseWork;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE__COURSE_WORK, oldCourseWork, newCourseWork);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseWork(CourseWork newCourseWork) {
		if (newCourseWork != courseWork) {
			NotificationChain msgs = null;
			if (courseWork != null)
				msgs = ((InternalEObject)courseWork).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CourseswebPackage.COURSE__COURSE_WORK, null, msgs);
			if (newCourseWork != null)
				msgs = ((InternalEObject)newCourseWork).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CourseswebPackage.COURSE__COURSE_WORK, null, msgs);
			msgs = basicSetCourseWork(newCourseWork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.COURSE__COURSE_WORK, newCourseWork, newCourseWork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EvaluationFormEntry> getEvaluationForm() {
		if (evaluationForm == null) {
			evaluationForm = new EObjectContainmentEList<EvaluationFormEntry>(EvaluationFormEntry.class, this, CourseswebPackage.COURSE__EVALUATION_FORM);
		}
		return evaluationForm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CourseswebPackage.COURSE__COURSE_INSTANCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourseInstances()).basicAdd(otherEnd, msgs);
			case CourseswebPackage.COURSE__CREDIT_REDUCTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCreditReductions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CourseswebPackage.COURSE__COURSE_INSTANCES:
				return ((InternalEList<?>)getCourseInstances()).basicRemove(otherEnd, msgs);
			case CourseswebPackage.COURSE__CREDIT_REDUCTIONS:
				return ((InternalEList<?>)getCreditReductions()).basicRemove(otherEnd, msgs);
			case CourseswebPackage.COURSE__COURSE_WORK:
				return basicSetCourseWork(null, msgs);
			case CourseswebPackage.COURSE__EVALUATION_FORM:
				return ((InternalEList<?>)getEvaluationForm()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CourseswebPackage.COURSE__CODE:
				return getCode();
			case CourseswebPackage.COURSE__NAME:
				return getName();
			case CourseswebPackage.COURSE__CONTENT:
				return getContent();
			case CourseswebPackage.COURSE__CREDITS:
				return getCredits();
			case CourseswebPackage.COURSE__COURSE_INSTANCES:
				return getCourseInstances();
			case CourseswebPackage.COURSE__REQUIRED_COURSES:
				return getRequiredCourses();
			case CourseswebPackage.COURSE__RECOMMENDED_COURSES:
				return getRecommendedCourses();
			case CourseswebPackage.COURSE__CREDIT_REDUCTIONS:
				return getCreditReductions();
			case CourseswebPackage.COURSE__COURSE_WORK:
				return getCourseWork();
			case CourseswebPackage.COURSE__EVALUATION_FORM:
				return getEvaluationForm();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CourseswebPackage.COURSE__CODE:
				setCode((String)newValue);
				return;
			case CourseswebPackage.COURSE__NAME:
				setName((String)newValue);
				return;
			case CourseswebPackage.COURSE__CONTENT:
				setContent((String)newValue);
				return;
			case CourseswebPackage.COURSE__CREDITS:
				setCredits((Float)newValue);
				return;
			case CourseswebPackage.COURSE__COURSE_INSTANCES:
				getCourseInstances().clear();
				getCourseInstances().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case CourseswebPackage.COURSE__REQUIRED_COURSES:
				getRequiredCourses().clear();
				getRequiredCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case CourseswebPackage.COURSE__RECOMMENDED_COURSES:
				getRecommendedCourses().clear();
				getRecommendedCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case CourseswebPackage.COURSE__CREDIT_REDUCTIONS:
				getCreditReductions().clear();
				getCreditReductions().addAll((Collection<? extends CreditReduction>)newValue);
				return;
			case CourseswebPackage.COURSE__COURSE_WORK:
				setCourseWork((CourseWork)newValue);
				return;
			case CourseswebPackage.COURSE__EVALUATION_FORM:
				getEvaluationForm().clear();
				getEvaluationForm().addAll((Collection<? extends EvaluationFormEntry>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CourseswebPackage.COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case CourseswebPackage.COURSE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CourseswebPackage.COURSE__CONTENT:
				setContent(CONTENT_EDEFAULT);
				return;
			case CourseswebPackage.COURSE__CREDITS:
				setCredits(CREDITS_EDEFAULT);
				return;
			case CourseswebPackage.COURSE__COURSE_INSTANCES:
				getCourseInstances().clear();
				return;
			case CourseswebPackage.COURSE__REQUIRED_COURSES:
				getRequiredCourses().clear();
				return;
			case CourseswebPackage.COURSE__RECOMMENDED_COURSES:
				getRecommendedCourses().clear();
				return;
			case CourseswebPackage.COURSE__CREDIT_REDUCTIONS:
				getCreditReductions().clear();
				return;
			case CourseswebPackage.COURSE__COURSE_WORK:
				setCourseWork((CourseWork)null);
				return;
			case CourseswebPackage.COURSE__EVALUATION_FORM:
				getEvaluationForm().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CourseswebPackage.COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case CourseswebPackage.COURSE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CourseswebPackage.COURSE__CONTENT:
				return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
			case CourseswebPackage.COURSE__CREDITS:
				return credits != CREDITS_EDEFAULT;
			case CourseswebPackage.COURSE__COURSE_INSTANCES:
				return courseInstances != null && !courseInstances.isEmpty();
			case CourseswebPackage.COURSE__REQUIRED_COURSES:
				return requiredCourses != null && !requiredCourses.isEmpty();
			case CourseswebPackage.COURSE__RECOMMENDED_COURSES:
				return recommendedCourses != null && !recommendedCourses.isEmpty();
			case CourseswebPackage.COURSE__CREDIT_REDUCTIONS:
				return creditReductions != null && !creditReductions.isEmpty();
			case CourseswebPackage.COURSE__COURSE_WORK:
				return courseWork != null;
			case CourseswebPackage.COURSE__EVALUATION_FORM:
				return evaluationForm != null && !evaluationForm.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(", name: ");
		result.append(name);
		result.append(", content: ");
		result.append(content);
		result.append(", credits: ");
		result.append(credits);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
