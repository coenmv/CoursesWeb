/**
 */
package tdt4250.coenmv.coursesweb.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import tdt4250.coenmv.coursesweb.CourseswebPackage;
import tdt4250.coenmv.coursesweb.EClassType;
import tdt4250.coenmv.coursesweb.EWeekDay;
import tdt4250.coenmv.coursesweb.Room;
import tdt4250.coenmv.coursesweb.StudyProgram;
import tdt4250.coenmv.coursesweb.TimetableEntry;

import tdt4250.coenmv.coursesweb.util.Timeslot;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timetable Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl#getWeekDay <em>Week Day</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl#getTimeslot <em>Timeslot</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl#getClassType <em>Class Type</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl#getStudyPrograms <em>Study Programs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimetableEntryImpl extends MinimalEObjectImpl.Container implements TimetableEntry {
	/**
	 * The default value of the '{@link #getWeekDay() <em>Week Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeekDay()
	 * @generated
	 * @ordered
	 */
	protected static final EWeekDay WEEK_DAY_EDEFAULT = EWeekDay.MONDAY;

	/**
	 * The cached value of the '{@link #getWeekDay() <em>Week Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeekDay()
	 * @generated
	 * @ordered
	 */
	protected EWeekDay weekDay = WEEK_DAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeslot() <em>Timeslot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeslot()
	 * @generated
	 * @ordered
	 */
	protected static final Timeslot TIMESLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeslot() <em>Timeslot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeslot()
	 * @generated
	 * @ordered
	 */
	protected Timeslot timeslot = TIMESLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getClassType() <em>Class Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassType()
	 * @generated
	 * @ordered
	 */
	protected static final EClassType CLASS_TYPE_EDEFAULT = EClassType.LECTURE;

	/**
	 * The cached value of the '{@link #getClassType() <em>Class Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassType()
	 * @generated
	 * @ordered
	 */
	protected EClassType classType = CLASS_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected Room room;

	/**
	 * The cached value of the '{@link #getStudyPrograms() <em>Study Programs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyPrograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyPrograms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimetableEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseswebPackage.Literals.TIMETABLE_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EWeekDay getWeekDay() {
		return weekDay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeekDay(EWeekDay newWeekDay) {
		EWeekDay oldWeekDay = weekDay;
		weekDay = newWeekDay == null ? WEEK_DAY_EDEFAULT : newWeekDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.TIMETABLE_ENTRY__WEEK_DAY, oldWeekDay, weekDay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timeslot getTimeslot() {
		return timeslot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeslot(Timeslot newTimeslot) {
		Timeslot oldTimeslot = timeslot;
		timeslot = newTimeslot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.TIMETABLE_ENTRY__TIMESLOT, oldTimeslot, timeslot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClassType getClassType() {
		return classType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassType(EClassType newClassType) {
		EClassType oldClassType = classType;
		classType = newClassType == null ? CLASS_TYPE_EDEFAULT : newClassType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.TIMETABLE_ENTRY__CLASS_TYPE, oldClassType, classType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getRoom() {
		if (room != null && room.eIsProxy()) {
			InternalEObject oldRoom = (InternalEObject)room;
			room = (Room)eResolveProxy(oldRoom);
			if (room != oldRoom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CourseswebPackage.TIMETABLE_ENTRY__ROOM, oldRoom, room));
			}
		}
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room basicGetRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(Room newRoom) {
		Room oldRoom = room;
		room = newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.TIMETABLE_ENTRY__ROOM, oldRoom, room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getStudyPrograms() {
		if (studyPrograms == null) {
			studyPrograms = new EObjectResolvingEList<StudyProgram>(StudyProgram.class, this, CourseswebPackage.TIMETABLE_ENTRY__STUDY_PROGRAMS);
		}
		return studyPrograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CourseswebPackage.TIMETABLE_ENTRY__WEEK_DAY:
				return getWeekDay();
			case CourseswebPackage.TIMETABLE_ENTRY__TIMESLOT:
				return getTimeslot();
			case CourseswebPackage.TIMETABLE_ENTRY__CLASS_TYPE:
				return getClassType();
			case CourseswebPackage.TIMETABLE_ENTRY__ROOM:
				if (resolve) return getRoom();
				return basicGetRoom();
			case CourseswebPackage.TIMETABLE_ENTRY__STUDY_PROGRAMS:
				return getStudyPrograms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CourseswebPackage.TIMETABLE_ENTRY__WEEK_DAY:
				setWeekDay((EWeekDay)newValue);
				return;
			case CourseswebPackage.TIMETABLE_ENTRY__TIMESLOT:
				setTimeslot((Timeslot)newValue);
				return;
			case CourseswebPackage.TIMETABLE_ENTRY__CLASS_TYPE:
				setClassType((EClassType)newValue);
				return;
			case CourseswebPackage.TIMETABLE_ENTRY__ROOM:
				setRoom((Room)newValue);
				return;
			case CourseswebPackage.TIMETABLE_ENTRY__STUDY_PROGRAMS:
				getStudyPrograms().clear();
				getStudyPrograms().addAll((Collection<? extends StudyProgram>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CourseswebPackage.TIMETABLE_ENTRY__WEEK_DAY:
				setWeekDay(WEEK_DAY_EDEFAULT);
				return;
			case CourseswebPackage.TIMETABLE_ENTRY__TIMESLOT:
				setTimeslot(TIMESLOT_EDEFAULT);
				return;
			case CourseswebPackage.TIMETABLE_ENTRY__CLASS_TYPE:
				setClassType(CLASS_TYPE_EDEFAULT);
				return;
			case CourseswebPackage.TIMETABLE_ENTRY__ROOM:
				setRoom((Room)null);
				return;
			case CourseswebPackage.TIMETABLE_ENTRY__STUDY_PROGRAMS:
				getStudyPrograms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CourseswebPackage.TIMETABLE_ENTRY__WEEK_DAY:
				return weekDay != WEEK_DAY_EDEFAULT;
			case CourseswebPackage.TIMETABLE_ENTRY__TIMESLOT:
				return TIMESLOT_EDEFAULT == null ? timeslot != null : !TIMESLOT_EDEFAULT.equals(timeslot);
			case CourseswebPackage.TIMETABLE_ENTRY__CLASS_TYPE:
				return classType != CLASS_TYPE_EDEFAULT;
			case CourseswebPackage.TIMETABLE_ENTRY__ROOM:
				return room != null;
			case CourseswebPackage.TIMETABLE_ENTRY__STUDY_PROGRAMS:
				return studyPrograms != null && !studyPrograms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (weekDay: ");
		result.append(weekDay);
		result.append(", timeslot: ");
		result.append(timeslot);
		result.append(", classType: ");
		result.append(classType);
		result.append(')');
		return result.toString();
	}

} //TimetableEntryImpl
