/**
 */
package tdt4250.coenmv.coursesweb.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import tdt4250.coenmv.coursesweb.CourseswebPackage;
import tdt4250.coenmv.coursesweb.EvaluationFormEntry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluation Form Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.EvaluationFormEntryImpl#getWeighting <em>Weighting</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.impl.EvaluationFormEntryImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationFormEntryImpl extends MinimalEObjectImpl.Container implements EvaluationFormEntry {
	/**
	 * The default value of the '{@link #getWeighting() <em>Weighting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeighting()
	 * @generated
	 * @ordered
	 */
	protected static final int WEIGHTING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWeighting() <em>Weighting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeighting()
	 * @generated
	 * @ordered
	 */
	protected int weighting = WEIGHTING_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationFormEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseswebPackage.Literals.EVALUATION_FORM_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWeighting() {
		return weighting;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeighting(int newWeighting) {
		int oldWeighting = weighting;
		weighting = newWeighting;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.EVALUATION_FORM_ENTRY__WEIGHTING, oldWeighting, weighting));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseswebPackage.EVALUATION_FORM_ENTRY__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CourseswebPackage.EVALUATION_FORM_ENTRY__WEIGHTING:
				return getWeighting();
			case CourseswebPackage.EVALUATION_FORM_ENTRY__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CourseswebPackage.EVALUATION_FORM_ENTRY__WEIGHTING:
				setWeighting((Integer)newValue);
				return;
			case CourseswebPackage.EVALUATION_FORM_ENTRY__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CourseswebPackage.EVALUATION_FORM_ENTRY__WEIGHTING:
				setWeighting(WEIGHTING_EDEFAULT);
				return;
			case CourseswebPackage.EVALUATION_FORM_ENTRY__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CourseswebPackage.EVALUATION_FORM_ENTRY__WEIGHTING:
				return weighting != WEIGHTING_EDEFAULT;
			case CourseswebPackage.EVALUATION_FORM_ENTRY__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (weighting: ");
		result.append(weighting);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //EvaluationFormEntryImpl
