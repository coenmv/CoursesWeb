/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import tdt4250.coenmv.coursesweb.util.Timeslot;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timetable Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.TimetableEntry#getWeekDay <em>Week Day</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.TimetableEntry#getTimeslot <em>Timeslot</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.TimetableEntry#getClassType <em>Class Type</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.TimetableEntry#getRoom <em>Room</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.TimetableEntry#getStudyPrograms <em>Study Programs</em>}</li>
 * </ul>
 *
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getTimetableEntry()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='timeslotWithinRange'"
 * @generated
 */
public interface TimetableEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Week Day</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.coenmv.coursesweb.EWeekDay}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Week Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Week Day</em>' attribute.
	 * @see tdt4250.coenmv.coursesweb.EWeekDay
	 * @see #setWeekDay(EWeekDay)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getTimetableEntry_WeekDay()
	 * @model
	 * @generated
	 */
	EWeekDay getWeekDay();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getWeekDay <em>Week Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Week Day</em>' attribute.
	 * @see tdt4250.coenmv.coursesweb.EWeekDay
	 * @see #getWeekDay()
	 * @generated
	 */
	void setWeekDay(EWeekDay value);

	/**
	 * Returns the value of the '<em><b>Timeslot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timeslot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timeslot</em>' attribute.
	 * @see #setTimeslot(Timeslot)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getTimetableEntry_Timeslot()
	 * @model dataType="tdt4250.coenmv.coursesweb.ETimeslot"
	 * @generated
	 */
	Timeslot getTimeslot();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getTimeslot <em>Timeslot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timeslot</em>' attribute.
	 * @see #getTimeslot()
	 * @generated
	 */
	void setTimeslot(Timeslot value);

	/**
	 * Returns the value of the '<em><b>Class Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.coenmv.coursesweb.EClassType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Type</em>' attribute.
	 * @see tdt4250.coenmv.coursesweb.EClassType
	 * @see #setClassType(EClassType)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getTimetableEntry_ClassType()
	 * @model
	 * @generated
	 */
	EClassType getClassType();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getClassType <em>Class Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Type</em>' attribute.
	 * @see tdt4250.coenmv.coursesweb.EClassType
	 * @see #getClassType()
	 * @generated
	 */
	void setClassType(EClassType value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' reference.
	 * @see #setRoom(Room)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getTimetableEntry_Room()
	 * @model
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getRoom <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' reference.
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

	/**
	 * Returns the value of the '<em><b>Study Programs</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Study Programs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Programs</em>' reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getTimetableEntry_StudyPrograms()
	 * @model
	 * @generated
	 */
	EList<StudyProgram> getStudyPrograms();

} // TimetableEntry
