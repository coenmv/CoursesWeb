/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250.coenmv.coursesweb.CourseswebFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0 http://www.eclipse.org/emf/2002/Ecore/OCL'"
 * @generated
 */
public interface CourseswebPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "coursesweb";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/tdt4250.coenmv.coursesweb/model/coursesweb.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "coursesweb";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CourseswebPackage eINSTANCE = tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.UniversityImpl <em>University</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.UniversityImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getUniversity()
	 * @generated
	 */
	int UNIVERSITY = 0;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__COURSES = 0;

	/**
	 * The feature id for the '<em><b>Departments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__DEPARTMENTS = 1;

	/**
	 * The feature id for the '<em><b>Persons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__PERSONS = 2;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__ROOMS = 3;

	/**
	 * The feature id for the '<em><b>Study Programs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__STUDY_PROGRAMS = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY__NAME = 5;

	/**
	 * The number of structural features of the '<em>University</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>University</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.CourseImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 1;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CONTENT = 2;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = 3;

	/**
	 * The feature id for the '<em><b>Course Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_INSTANCES = 4;

	/**
	 * The feature id for the '<em><b>Required Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__REQUIRED_COURSES = 5;

	/**
	 * The feature id for the '<em><b>Recommended Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__RECOMMENDED_COURSES = 6;

	/**
	 * The feature id for the '<em><b>Credit Reductions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDIT_REDUCTIONS = 7;

	/**
	 * The feature id for the '<em><b>Course Work</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_WORK = 8;

	/**
	 * The feature id for the '<em><b>Evaluation Form</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__EVALUATION_FORM = 9;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getCourseInstance()
	 * @generated
	 */
	int COURSE_INSTANCE = 2;

	/**
	 * The feature id for the '<em><b>Timetable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__TIMETABLE = 0;

	/**
	 * The feature id for the '<em><b>Responsible Department</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__RESPONSIBLE_DEPARTMENT = 1;

	/**
	 * The feature id for the '<em><b>Staff</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__STAFF = 2;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__YEAR = 3;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__SEMESTER = 4;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE = 5;

	/**
	 * The number of structural features of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.StaffImpl <em>Staff</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.StaffImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getStaff()
	 * @generated
	 */
	int STAFF = 3;

	/**
	 * The feature id for the '<em><b>Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF__ROLE = 0;

	/**
	 * The feature id for the '<em><b>Person</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF__PERSON = 1;

	/**
	 * The number of structural features of the '<em>Staff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Staff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.DepartmentImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 8;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl <em>Timetable Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getTimetableEntry()
	 * @generated
	 */
	int TIMETABLE_ENTRY = 4;

	/**
	 * The feature id for the '<em><b>Week Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_ENTRY__WEEK_DAY = 0;

	/**
	 * The feature id for the '<em><b>Timeslot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_ENTRY__TIMESLOT = 1;

	/**
	 * The feature id for the '<em><b>Class Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_ENTRY__CLASS_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_ENTRY__ROOM = 3;

	/**
	 * The feature id for the '<em><b>Study Programs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_ENTRY__STUDY_PROGRAMS = 4;

	/**
	 * The number of structural features of the '<em>Timetable Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_ENTRY_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Timetable Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.EvaluationFormEntryImpl <em>Evaluation Form Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.EvaluationFormEntryImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getEvaluationFormEntry()
	 * @generated
	 */
	int EVALUATION_FORM_ENTRY = 5;

	/**
	 * The feature id for the '<em><b>Weighting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM_ENTRY__WEIGHTING = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM_ENTRY__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Evaluation Form Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Evaluation Form Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.CreditReductionImpl <em>Credit Reduction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.CreditReductionImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getCreditReduction()
	 * @generated
	 */
	int CREDIT_REDUCTION = 6;

	/**
	 * The feature id for the '<em><b>Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION__REDUCTION = 0;

	/**
	 * The feature id for the '<em><b>By Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION__BY_COURSE = 1;

	/**
	 * The feature id for the '<em><b>On Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION__ON_COURSE = 2;

	/**
	 * The number of structural features of the '<em>Credit Reduction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Credit Reduction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.CourseWorkImpl <em>Course Work</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.CourseWorkImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getCourseWork()
	 * @generated
	 */
	int COURSE_WORK = 7;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LECTURE_HOURS = 0;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LAB_HOURS = 1;

	/**
	 * The number of structural features of the '<em>Course Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Course Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 1;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.PersonImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 9;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__FIRST_NAME = 0;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__LAST_NAME = 1;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.RoomImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__NAME = 0;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.impl.StudyProgramImpl <em>Study Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.impl.StudyProgramImpl
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getStudyProgram()
	 * @generated
	 */
	int STUDY_PROGRAM = 11;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__CODE = 0;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__COURSES = 1;

	/**
	 * The number of structural features of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.EStaffRole <em>EStaff Role</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.EStaffRole
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getEStaffRole()
	 * @generated
	 */
	int ESTAFF_ROLE = 12;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.ESemester <em>ESemester</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.ESemester
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getESemester()
	 * @generated
	 */
	int ESEMESTER = 13;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.EWeekDay <em>EWeek Day</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.EWeekDay
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getEWeekDay()
	 * @generated
	 */
	int EWEEK_DAY = 14;

	/**
	 * The meta object id for the '{@link tdt4250.coenmv.coursesweb.EClassType <em>EClass Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.EClassType
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getEClassType()
	 * @generated
	 */
	int ECLASS_TYPE = 15;

	/**
	 * The meta object id for the '<em>ETimeslot</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.coenmv.coursesweb.util.Timeslot
	 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getETimeslot()
	 * @generated
	 */
	int ETIMESLOT = 16;


	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.University <em>University</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>University</em>'.
	 * @see tdt4250.coenmv.coursesweb.University
	 * @generated
	 */
	EClass getUniversity();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.University#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see tdt4250.coenmv.coursesweb.University#getCourses()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_Courses();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.University#getDepartments <em>Departments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Departments</em>'.
	 * @see tdt4250.coenmv.coursesweb.University#getDepartments()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_Departments();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.University#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Persons</em>'.
	 * @see tdt4250.coenmv.coursesweb.University#getPersons()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_Persons();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.University#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rooms</em>'.
	 * @see tdt4250.coenmv.coursesweb.University#getRooms()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_Rooms();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.University#getStudyPrograms <em>Study Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Study Programs</em>'.
	 * @see tdt4250.coenmv.coursesweb.University#getStudyPrograms()
	 * @see #getUniversity()
	 * @generated
	 */
	EReference getUniversity_StudyPrograms();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.University#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.coenmv.coursesweb.University#getName()
	 * @see #getUniversity()
	 * @generated
	 */
	EAttribute getUniversity_Name();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Course#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getContent()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Content();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.Course#getCourseInstances <em>Course Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Course Instances</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getCourseInstances()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseInstances();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.coenmv.coursesweb.Course#getRequiredCourses <em>Required Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Courses</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getRequiredCourses()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_RequiredCourses();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.coenmv.coursesweb.Course#getRecommendedCourses <em>Recommended Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Recommended Courses</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getRecommendedCourses()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_RecommendedCourses();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.Course#getCreditReductions <em>Credit Reductions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Credit Reductions</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getCreditReductions()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CreditReductions();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.coenmv.coursesweb.Course#getCourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Course Work</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getCourseWork()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseWork();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.Course#getEvaluationForm <em>Evaluation Form</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Evaluation Form</em>'.
	 * @see tdt4250.coenmv.coursesweb.Course#getEvaluationForm()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_EvaluationForm();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.CourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Instance</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseInstance
	 * @generated
	 */
	EClass getCourseInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.CourseInstance#getTimetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timetable</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseInstance#getTimetable()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Timetable();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.coenmv.coursesweb.CourseInstance#getResponsibleDepartment <em>Responsible Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Responsible Department</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseInstance#getResponsibleDepartment()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_ResponsibleDepartment();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.coenmv.coursesweb.CourseInstance#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Staff</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseInstance#getStaff()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Staff();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.CourseInstance#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseInstance#getYear()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Year();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.CourseInstance#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseInstance#getSemester()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Semester();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.coenmv.coursesweb.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseInstance#getCourse()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Course();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.Staff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Staff</em>'.
	 * @see tdt4250.coenmv.coursesweb.Staff
	 * @generated
	 */
	EClass getStaff();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Staff#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role</em>'.
	 * @see tdt4250.coenmv.coursesweb.Staff#getRole()
	 * @see #getStaff()
	 * @generated
	 */
	EAttribute getStaff_Role();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.coenmv.coursesweb.Staff#getPerson <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Person</em>'.
	 * @see tdt4250.coenmv.coursesweb.Staff#getPerson()
	 * @see #getStaff()
	 * @generated
	 */
	EReference getStaff_Person();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see tdt4250.coenmv.coursesweb.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.coenmv.coursesweb.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Department#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see tdt4250.coenmv.coursesweb.Department#getCode()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Code();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.TimetableEntry <em>Timetable Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timetable Entry</em>'.
	 * @see tdt4250.coenmv.coursesweb.TimetableEntry
	 * @generated
	 */
	EClass getTimetableEntry();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getWeekDay <em>Week Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Week Day</em>'.
	 * @see tdt4250.coenmv.coursesweb.TimetableEntry#getWeekDay()
	 * @see #getTimetableEntry()
	 * @generated
	 */
	EAttribute getTimetableEntry_WeekDay();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getTimeslot <em>Timeslot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timeslot</em>'.
	 * @see tdt4250.coenmv.coursesweb.TimetableEntry#getTimeslot()
	 * @see #getTimetableEntry()
	 * @generated
	 */
	EAttribute getTimetableEntry_Timeslot();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getClassType <em>Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class Type</em>'.
	 * @see tdt4250.coenmv.coursesweb.TimetableEntry#getClassType()
	 * @see #getTimetableEntry()
	 * @generated
	 */
	EAttribute getTimetableEntry_ClassType();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room</em>'.
	 * @see tdt4250.coenmv.coursesweb.TimetableEntry#getRoom()
	 * @see #getTimetableEntry()
	 * @generated
	 */
	EReference getTimetableEntry_Room();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.coenmv.coursesweb.TimetableEntry#getStudyPrograms <em>Study Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Study Programs</em>'.
	 * @see tdt4250.coenmv.coursesweb.TimetableEntry#getStudyPrograms()
	 * @see #getTimetableEntry()
	 * @generated
	 */
	EReference getTimetableEntry_StudyPrograms();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.EvaluationFormEntry <em>Evaluation Form Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation Form Entry</em>'.
	 * @see tdt4250.coenmv.coursesweb.EvaluationFormEntry
	 * @generated
	 */
	EClass getEvaluationFormEntry();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.EvaluationFormEntry#getWeighting <em>Weighting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Weighting</em>'.
	 * @see tdt4250.coenmv.coursesweb.EvaluationFormEntry#getWeighting()
	 * @see #getEvaluationFormEntry()
	 * @generated
	 */
	EAttribute getEvaluationFormEntry_Weighting();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.EvaluationFormEntry#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see tdt4250.coenmv.coursesweb.EvaluationFormEntry#getType()
	 * @see #getEvaluationFormEntry()
	 * @generated
	 */
	EAttribute getEvaluationFormEntry_Type();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.CreditReduction <em>Credit Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Credit Reduction</em>'.
	 * @see tdt4250.coenmv.coursesweb.CreditReduction
	 * @generated
	 */
	EClass getCreditReduction();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.CreditReduction#getReduction <em>Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reduction</em>'.
	 * @see tdt4250.coenmv.coursesweb.CreditReduction#getReduction()
	 * @see #getCreditReduction()
	 * @generated
	 */
	EAttribute getCreditReduction_Reduction();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.coenmv.coursesweb.CreditReduction#getByCourse <em>By Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>By Course</em>'.
	 * @see tdt4250.coenmv.coursesweb.CreditReduction#getByCourse()
	 * @see #getCreditReduction()
	 * @generated
	 */
	EReference getCreditReduction_ByCourse();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.coenmv.coursesweb.CreditReduction#getOnCourse <em>On Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>On Course</em>'.
	 * @see tdt4250.coenmv.coursesweb.CreditReduction#getOnCourse()
	 * @see #getCreditReduction()
	 * @generated
	 */
	EReference getCreditReduction_OnCourse();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.CourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Work</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseWork
	 * @generated
	 */
	EClass getCourseWork();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.CourseWork#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Hours</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseWork#getLectureHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LectureHours();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.CourseWork#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab Hours</em>'.
	 * @see tdt4250.coenmv.coursesweb.CourseWork#getLabHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LabHours();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see tdt4250.coenmv.coursesweb.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Person#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see tdt4250.coenmv.coursesweb.Person#getFirstName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Person#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see tdt4250.coenmv.coursesweb.Person#getLastName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_LastName();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see tdt4250.coenmv.coursesweb.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.Room#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.coenmv.coursesweb.Room#getName()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Name();

	/**
	 * Returns the meta object for class '{@link tdt4250.coenmv.coursesweb.StudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Program</em>'.
	 * @see tdt4250.coenmv.coursesweb.StudyProgram
	 * @generated
	 */
	EClass getStudyProgram();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.coenmv.coursesweb.StudyProgram#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see tdt4250.coenmv.coursesweb.StudyProgram#getCode()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_Code();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.coenmv.coursesweb.StudyProgram#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Courses</em>'.
	 * @see tdt4250.coenmv.coursesweb.StudyProgram#getCourses()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_Courses();

	/**
	 * Returns the meta object for enum '{@link tdt4250.coenmv.coursesweb.EStaffRole <em>EStaff Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EStaff Role</em>'.
	 * @see tdt4250.coenmv.coursesweb.EStaffRole
	 * @generated
	 */
	EEnum getEStaffRole();

	/**
	 * Returns the meta object for enum '{@link tdt4250.coenmv.coursesweb.ESemester <em>ESemester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ESemester</em>'.
	 * @see tdt4250.coenmv.coursesweb.ESemester
	 * @generated
	 */
	EEnum getESemester();

	/**
	 * Returns the meta object for enum '{@link tdt4250.coenmv.coursesweb.EWeekDay <em>EWeek Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EWeek Day</em>'.
	 * @see tdt4250.coenmv.coursesweb.EWeekDay
	 * @generated
	 */
	EEnum getEWeekDay();

	/**
	 * Returns the meta object for enum '{@link tdt4250.coenmv.coursesweb.EClassType <em>EClass Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EClass Type</em>'.
	 * @see tdt4250.coenmv.coursesweb.EClassType
	 * @generated
	 */
	EEnum getEClassType();

	/**
	 * Returns the meta object for data type '{@link tdt4250.coenmv.coursesweb.util.Timeslot <em>ETimeslot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ETimeslot</em>'.
	 * @see tdt4250.coenmv.coursesweb.util.Timeslot
	 * @model instanceClass="tdt4250.coenmv.coursesweb.util.Timeslot"
	 * @generated
	 */
	EDataType getETimeslot();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CourseswebFactory getCourseswebFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.UniversityImpl <em>University</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.UniversityImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getUniversity()
		 * @generated
		 */
		EClass UNIVERSITY = eINSTANCE.getUniversity();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__COURSES = eINSTANCE.getUniversity_Courses();

		/**
		 * The meta object literal for the '<em><b>Departments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__DEPARTMENTS = eINSTANCE.getUniversity_Departments();

		/**
		 * The meta object literal for the '<em><b>Persons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__PERSONS = eINSTANCE.getUniversity_Persons();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__ROOMS = eINSTANCE.getUniversity_Rooms();

		/**
		 * The meta object literal for the '<em><b>Study Programs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIVERSITY__STUDY_PROGRAMS = eINSTANCE.getUniversity_StudyPrograms();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIVERSITY__NAME = eINSTANCE.getUniversity_Name();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.CourseImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CONTENT = eINSTANCE.getCourse_Content();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '<em><b>Course Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_INSTANCES = eINSTANCE.getCourse_CourseInstances();

		/**
		 * The meta object literal for the '<em><b>Required Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__REQUIRED_COURSES = eINSTANCE.getCourse_RequiredCourses();

		/**
		 * The meta object literal for the '<em><b>Recommended Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__RECOMMENDED_COURSES = eINSTANCE.getCourse_RecommendedCourses();

		/**
		 * The meta object literal for the '<em><b>Credit Reductions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__CREDIT_REDUCTIONS = eINSTANCE.getCourse_CreditReductions();

		/**
		 * The meta object literal for the '<em><b>Course Work</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_WORK = eINSTANCE.getCourse_CourseWork();

		/**
		 * The meta object literal for the '<em><b>Evaluation Form</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__EVALUATION_FORM = eINSTANCE.getCourse_EvaluationForm();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.CourseInstanceImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getCourseInstance()
		 * @generated
		 */
		EClass COURSE_INSTANCE = eINSTANCE.getCourseInstance();

		/**
		 * The meta object literal for the '<em><b>Timetable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__TIMETABLE = eINSTANCE.getCourseInstance_Timetable();

		/**
		 * The meta object literal for the '<em><b>Responsible Department</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__RESPONSIBLE_DEPARTMENT = eINSTANCE.getCourseInstance_ResponsibleDepartment();

		/**
		 * The meta object literal for the '<em><b>Staff</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__STAFF = eINSTANCE.getCourseInstance_Staff();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__YEAR = eINSTANCE.getCourseInstance_Year();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__SEMESTER = eINSTANCE.getCourseInstance_Semester();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE = eINSTANCE.getCourseInstance_Course();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.StaffImpl <em>Staff</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.StaffImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getStaff()
		 * @generated
		 */
		EClass STAFF = eINSTANCE.getStaff();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STAFF__ROLE = eINSTANCE.getStaff_Role();

		/**
		 * The meta object literal for the '<em><b>Person</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STAFF__PERSON = eINSTANCE.getStaff_Person();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.DepartmentImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__CODE = eINSTANCE.getDepartment_Code();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl <em>Timetable Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.TimetableEntryImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getTimetableEntry()
		 * @generated
		 */
		EClass TIMETABLE_ENTRY = eINSTANCE.getTimetableEntry();

		/**
		 * The meta object literal for the '<em><b>Week Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE_ENTRY__WEEK_DAY = eINSTANCE.getTimetableEntry_WeekDay();

		/**
		 * The meta object literal for the '<em><b>Timeslot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE_ENTRY__TIMESLOT = eINSTANCE.getTimetableEntry_Timeslot();

		/**
		 * The meta object literal for the '<em><b>Class Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE_ENTRY__CLASS_TYPE = eINSTANCE.getTimetableEntry_ClassType();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE_ENTRY__ROOM = eINSTANCE.getTimetableEntry_Room();

		/**
		 * The meta object literal for the '<em><b>Study Programs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE_ENTRY__STUDY_PROGRAMS = eINSTANCE.getTimetableEntry_StudyPrograms();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.EvaluationFormEntryImpl <em>Evaluation Form Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.EvaluationFormEntryImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getEvaluationFormEntry()
		 * @generated
		 */
		EClass EVALUATION_FORM_ENTRY = eINSTANCE.getEvaluationFormEntry();

		/**
		 * The meta object literal for the '<em><b>Weighting</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_FORM_ENTRY__WEIGHTING = eINSTANCE.getEvaluationFormEntry_Weighting();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_FORM_ENTRY__TYPE = eINSTANCE.getEvaluationFormEntry_Type();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.CreditReductionImpl <em>Credit Reduction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.CreditReductionImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getCreditReduction()
		 * @generated
		 */
		EClass CREDIT_REDUCTION = eINSTANCE.getCreditReduction();

		/**
		 * The meta object literal for the '<em><b>Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_REDUCTION__REDUCTION = eINSTANCE.getCreditReduction_Reduction();

		/**
		 * The meta object literal for the '<em><b>By Course</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CREDIT_REDUCTION__BY_COURSE = eINSTANCE.getCreditReduction_ByCourse();

		/**
		 * The meta object literal for the '<em><b>On Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CREDIT_REDUCTION__ON_COURSE = eINSTANCE.getCreditReduction_OnCourse();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.CourseWorkImpl <em>Course Work</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.CourseWorkImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getCourseWork()
		 * @generated
		 */
		EClass COURSE_WORK = eINSTANCE.getCourseWork();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LECTURE_HOURS = eINSTANCE.getCourseWork_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LAB_HOURS = eINSTANCE.getCourseWork_LabHours();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.PersonImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__FIRST_NAME = eINSTANCE.getPerson_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__LAST_NAME = eINSTANCE.getPerson_LastName();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.RoomImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__NAME = eINSTANCE.getRoom_Name();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.impl.StudyProgramImpl <em>Study Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.impl.StudyProgramImpl
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getStudyProgram()
		 * @generated
		 */
		EClass STUDY_PROGRAM = eINSTANCE.getStudyProgram();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__CODE = eINSTANCE.getStudyProgram_Code();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__COURSES = eINSTANCE.getStudyProgram_Courses();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.EStaffRole <em>EStaff Role</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.EStaffRole
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getEStaffRole()
		 * @generated
		 */
		EEnum ESTAFF_ROLE = eINSTANCE.getEStaffRole();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.ESemester <em>ESemester</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.ESemester
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getESemester()
		 * @generated
		 */
		EEnum ESEMESTER = eINSTANCE.getESemester();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.EWeekDay <em>EWeek Day</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.EWeekDay
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getEWeekDay()
		 * @generated
		 */
		EEnum EWEEK_DAY = eINSTANCE.getEWeekDay();

		/**
		 * The meta object literal for the '{@link tdt4250.coenmv.coursesweb.EClassType <em>EClass Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.EClassType
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getEClassType()
		 * @generated
		 */
		EEnum ECLASS_TYPE = eINSTANCE.getEClassType();

		/**
		 * The meta object literal for the '<em>ETimeslot</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.coenmv.coursesweb.util.Timeslot
		 * @see tdt4250.coenmv.coursesweb.impl.CourseswebPackageImpl#getETimeslot()
		 * @generated
		 */
		EDataType ETIMESLOT = eINSTANCE.getETimeslot();

	}

} //CourseswebPackage
