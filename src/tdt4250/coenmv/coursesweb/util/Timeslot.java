package tdt4250.coenmv.coursesweb.util;

public class Timeslot {
	
	private int startHours, startMinutes, endHours, endMinutes;

	public Timeslot(int startHours, int startMinutes, int endHours, int endMinutes) {
		super();
		this.startHours = startHours;
		this.startMinutes = startMinutes;
		this.endHours = endHours;
		this.endMinutes = endMinutes;
	}
	
	public int getStartHours() {
		return startHours;
	}

	public void setStartHours(int startHours) {
		this.startHours = startHours;
	}

	public int getStartMinutes() {
		return startMinutes;
	}

	public void setStartMinutes(int startMinutes) {
		this.startMinutes = startMinutes;
	}

	public int getEndHours() {
		return endHours;
	}

	public void setEndHours(int endHours) {
		this.endHours = endHours;
	}

	public int getEndMinutes() {
		return endMinutes;
	}

	public void setEndMinutes(int endMinutes) {
		this.endMinutes = endMinutes;
	}

	public int getHours() {
		int partialHour = (endMinutes - startMinutes) / 60;
		return endHours - startHours + partialHour;
	}
	
	@Override
	public String toString() {
		String startHoursString = (startHours < 10) ? "0" + startHours : Integer.toString(startHours);
		String startMinutesString = (startMinutes < 10) ? "0" + startMinutes : Integer.toString(startMinutes);
		String endHoursString = (endHours < 10) ? "0" + endHours : Integer.toString(endHours);
		String endMinutesString = (endMinutes < 10) ? "0" + endMinutes : Integer.toString(endMinutes);
		return startHoursString + ":" + startMinutesString + "-" + endHoursString + ":" + endMinutesString;
	}
	
	public boolean interFeresWith(Timeslot timeslot) {
		if (endHours < timeslot.startHours) {
			return false;
		}
		if (endHours == timeslot.startHours) {
			return endMinutes > timeslot.startMinutes;
		}
		if (startHours > timeslot.endHours) {
			return false;
		}
		if (startHours == timeslot.endHours) {
			return startMinutes < timeslot.endMinutes;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + endHours;
		result = prime * result + endMinutes;
		result = prime * result + startHours;
		result = prime * result + startMinutes;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Timeslot))
			return false;
		Timeslot other = (Timeslot) obj;
		if (endHours != other.endHours)
			return false;
		if (endMinutes != other.endMinutes)
			return false;
		if (startHours != other.startHours)
			return false;
		if (startMinutes != other.startMinutes)
			return false;
		return true;
	}

}
