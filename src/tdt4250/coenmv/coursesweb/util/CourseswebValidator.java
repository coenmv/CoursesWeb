/**
 */
package tdt4250.coenmv.coursesweb.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import tdt4250.coenmv.coursesweb.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage
 * @generated
 */
public class CourseswebValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final CourseswebValidator INSTANCE = new CourseswebValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "tdt4250.coenmv.coursesweb";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseswebValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return CourseswebPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case CourseswebPackage.UNIVERSITY:
				return validateUniversity((University)value, diagnostics, context);
			case CourseswebPackage.COURSE:
				return validateCourse((Course)value, diagnostics, context);
			case CourseswebPackage.COURSE_INSTANCE:
				return validateCourseInstance((CourseInstance)value, diagnostics, context);
			case CourseswebPackage.STAFF:
				return validateStaff((Staff)value, diagnostics, context);
			case CourseswebPackage.TIMETABLE_ENTRY:
				return validateTimetableEntry((TimetableEntry)value, diagnostics, context);
			case CourseswebPackage.EVALUATION_FORM_ENTRY:
				return validateEvaluationFormEntry((EvaluationFormEntry)value, diagnostics, context);
			case CourseswebPackage.CREDIT_REDUCTION:
				return validateCreditReduction((CreditReduction)value, diagnostics, context);
			case CourseswebPackage.COURSE_WORK:
				return validateCourseWork((CourseWork)value, diagnostics, context);
			case CourseswebPackage.DEPARTMENT:
				return validateDepartment((Department)value, diagnostics, context);
			case CourseswebPackage.PERSON:
				return validatePerson((Person)value, diagnostics, context);
			case CourseswebPackage.ROOM:
				return validateRoom((Room)value, diagnostics, context);
			case CourseswebPackage.STUDY_PROGRAM:
				return validateStudyProgram((StudyProgram)value, diagnostics, context);
			case CourseswebPackage.ESTAFF_ROLE:
				return validateEStaffRole((EStaffRole)value, diagnostics, context);
			case CourseswebPackage.ESEMESTER:
				return validateESemester((ESemester)value, diagnostics, context);
			case CourseswebPackage.EWEEK_DAY:
				return validateEWeekDay((EWeekDay)value, diagnostics, context);
			case CourseswebPackage.ECLASS_TYPE:
				return validateEClassType((EClassType)value, diagnostics, context);
			case CourseswebPackage.ETIMESLOT:
				return validateETimeslot((Timeslot)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversity(University university, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(university, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(university, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(university, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversity_uniqueCourseCodes(university, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversity_uniqueDepartmentCodes(university, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversity_uniqueStudyProgramCodes(university, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the uniqueCourseCodes constraint of '<em>University</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String UNIVERSITY__UNIQUE_COURSE_CODES__EEXPRESSION = "self.courses->forAll(c1, c2 | c1 <> c2 implies c1.code <> c2.code)";

	/**
	 * Validates the uniqueCourseCodes constraint of '<em>University</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversity_uniqueCourseCodes(University university, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.UNIVERSITY,
				 university,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "uniqueCourseCodes",
				 UNIVERSITY__UNIQUE_COURSE_CODES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the uniqueDepartmentCodes constraint of '<em>University</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String UNIVERSITY__UNIQUE_DEPARTMENT_CODES__EEXPRESSION = "self.departments->forAll(d1, d2 | d1 <> d2 implies d1.code <> d2.code)";

	/**
	 * Validates the uniqueDepartmentCodes constraint of '<em>University</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversity_uniqueDepartmentCodes(University university, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.UNIVERSITY,
				 university,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "uniqueDepartmentCodes",
				 UNIVERSITY__UNIQUE_DEPARTMENT_CODES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the uniqueStudyProgramCodes constraint of '<em>University</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String UNIVERSITY__UNIQUE_STUDY_PROGRAM_CODES__EEXPRESSION = "self.studyPrograms->forAll(sp1, sp2 | sp1 <> sp2 implies sp1.code <> sp2.code)";

	/**
	 * Validates the uniqueStudyProgramCodes constraint of '<em>University</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversity_uniqueStudyProgramCodes(University university, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.UNIVERSITY,
				 university,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "uniqueStudyProgramCodes",
				 UNIVERSITY__UNIQUE_STUDY_PROGRAM_CODES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(course, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(course, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourse_recommendedCoursesOther(course, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourse_requiredCoursesOther(course, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourse_courseInstancesDistinctYearSemesterPair(course, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourse_evaluationFormWeightingCorrectSum(course, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourse_nonNegativeCredits(course, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the recommendedCoursesOther constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE__RECOMMENDED_COURSES_OTHER__EEXPRESSION = "self.recommendedCourses->forAll(rc|rc<>self)";

	/**
	 * Validates the recommendedCoursesOther constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse_recommendedCoursesOther(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE,
				 course,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "recommendedCoursesOther",
				 COURSE__RECOMMENDED_COURSES_OTHER__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the requiredCoursesOther constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE__REQUIRED_COURSES_OTHER__EEXPRESSION = "self.requiredCourses->forAll(rc|rc<>self)";

	/**
	 * Validates the requiredCoursesOther constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse_requiredCoursesOther(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE,
				 course,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "requiredCoursesOther",
				 COURSE__REQUIRED_COURSES_OTHER__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the courseInstancesDistinctYearSemesterPair constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE__COURSE_INSTANCES_DISTINCT_YEAR_SEMESTER_PAIR__EEXPRESSION = "self.courseInstances->forAll(ci1, ci2 | ci1 <> ci2 implies (ci1.year <> ci2.year or ci1.semester <> ci2.semester))";

	/**
	 * Validates the courseInstancesDistinctYearSemesterPair constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse_courseInstancesDistinctYearSemesterPair(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE,
				 course,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "courseInstancesDistinctYearSemesterPair",
				 COURSE__COURSE_INSTANCES_DISTINCT_YEAR_SEMESTER_PAIR__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the evaluationFormWeightingCorrectSum constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE__EVALUATION_FORM_WEIGHTING_CORRECT_SUM__EEXPRESSION = "self.evaluationForm.weighting->sum()=100";

	/**
	 * Validates the evaluationFormWeightingCorrectSum constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse_evaluationFormWeightingCorrectSum(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE,
				 course,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "evaluationFormWeightingCorrectSum",
				 COURSE__EVALUATION_FORM_WEIGHTING_CORRECT_SUM__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the nonNegativeCredits constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE__NON_NEGATIVE_CREDITS__EEXPRESSION = "self.credits>=0.0";

	/**
	 * Validates the nonNegativeCredits constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse_nonNegativeCredits(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE,
				 course,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "nonNegativeCredits",
				 COURSE__NON_NEGATIVE_CREDITS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(courseInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_timetableScheduledLectureHoursMatch(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_timetableScheduledLabHoursMatch(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_timetableStudyProgramsProvideCourse(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_timetableEntriesNotInterfering(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_staffHasOneCourseCoordinator(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_staffNoDuplicateRoles(courseInstance, diagnostics, context);
		return result;
	}

	/**
	 * Validates the timetableScheduledLectureHoursMatch constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_timetableScheduledLectureHoursMatch(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		EList<TimetableEntry> timetable = courseInstance.getTimetable();
		int lectureHours = 0;
		for (int i = 0; i < timetable.size(); i++) {
			TimetableEntry timetableEntry = timetable.get(i);
			if (timetableEntry.getClassType() == EClassType.LECTURE) {
				if (timetableEntry.getTimeslot() != null) {
					lectureHours += timetableEntry.getTimeslot().getHours();
				}
			}
		}
		CourseWork courseWork = courseInstance.getCourse().getCourseWork();
		if (courseWork != null && lectureHours < courseWork.getLectureHours()) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "timetableScheduledLectureHoursMatch", getObjectLabel(courseInstance, context) },
						 new Object[] { courseInstance },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the timetableScheduledLabHoursMatch constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_timetableScheduledLabHoursMatch(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		EList<TimetableEntry> timetable = courseInstance.getTimetable();
		int labHours = 0;
		for (int i = 0; i < timetable.size(); i++) {
			TimetableEntry timetableEntry = timetable.get(i);
			if (timetableEntry.getClassType() == EClassType.LAB) {
				if (timetableEntry.getTimeslot() != null) {
					labHours += timetableEntry.getTimeslot().getHours();
				}
			}
		}
		CourseWork courseWork = courseInstance.getCourse().getCourseWork();
		if (courseWork != null && labHours < courseWork.getLabHours()) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "timetableScheduledLabHoursMatch", getObjectLabel(courseInstance, context) },
						 new Object[] { courseInstance },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * The cached validation expression for the timetableStudyProgramsProvideCourse constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE_INSTANCE__TIMETABLE_STUDY_PROGRAMS_PROVIDE_COURSE__EEXPRESSION = "self.timetable->forAll(te|te.studyPrograms->forAll(sp|sp.courses->includes(self.course)))";

	/**
	 * Validates the timetableStudyProgramsProvideCourse constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance_timetableStudyProgramsProvideCourse(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE_INSTANCE,
				 courseInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "timetableStudyProgramsProvideCourse",
				 COURSE_INSTANCE__TIMETABLE_STUDY_PROGRAMS_PROVIDE_COURSE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * Validates the timetableEntriesNotInterfering constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_timetableEntriesNotInterfering(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		EList<TimetableEntry> timetable = courseInstance.getTimetable();
		for (int i = 0; i < timetable.size(); i++) {
			TimetableEntry timetableEntry1 = timetable.get(i);
			for (int j = i; j < timetable.size(); j++) {
				TimetableEntry timetableEntry2 = timetable.get(j);
				if (timetableEntry1.getWeekDay() == timetableEntry2.getWeekDay()) {
					Timeslot timeslot1 = timetableEntry1.getTimeslot();
					Timeslot timeslot2 = timetableEntry2.getTimeslot();
					if (timeslot1 != null && timeslot2 != null && timeslot1.interFeresWith(timeslot2)) {
						if (timetableEntry1.getStudyPrograms().stream().anyMatch(timetableEntry2.getStudyPrograms()::contains)) {
							if (diagnostics != null) {
								diagnostics.add
									(createDiagnostic
										(Diagnostic.ERROR,
										 DIAGNOSTIC_SOURCE,
										 0,
										 "_UI_GenericConstraint_diagnostic",
										 new Object[] { "timetableEntriesNotInterfering", getObjectLabel(courseInstance, context) },
										 new Object[] { courseInstance },
										 context));
							}
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * The cached validation expression for the staffHasOneCourseCoordinator constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE_INSTANCE__STAFF_HAS_ONE_COURSE_COORDINATOR__EEXPRESSION = "self.staff->select(s|s.role = EStaffRole::COURSE_COORDINATOR)->size() = 1";

	/**
	 * Validates the staffHasOneCourseCoordinator constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance_staffHasOneCourseCoordinator(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE_INSTANCE,
				 courseInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "staffHasOneCourseCoordinator",
				 COURSE_INSTANCE__STAFF_HAS_ONE_COURSE_COORDINATOR__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the staffNoDuplicateRoles constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE_INSTANCE__STAFF_NO_DUPLICATE_ROLES__EEXPRESSION = "self.staff->forAll(s1, s2 | s1 <> s2 implies (s1.person <> s2.person or s1.role <> s2.role))";

	/**
	 * Validates the staffNoDuplicateRoles constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance_staffNoDuplicateRoles(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE_INSTANCE,
				 courseInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "staffNoDuplicateRoles",
				 COURSE_INSTANCE__STAFF_NO_DUPLICATE_ROLES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStaff(Staff staff, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(staff, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(department, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimetableEntry(TimetableEntry timetableEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(timetableEntry, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(timetableEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(timetableEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(timetableEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(timetableEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(timetableEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(timetableEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(timetableEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(timetableEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validateTimetableEntry_timeslotWithinRange(timetableEntry, diagnostics, context);
		return result;
	}

	/**
	 * Validates the timeslotWithinRange constraint of '<em>Timetable Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateTimetableEntry_timeslotWithinRange(TimetableEntry timetableEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		Timeslot timeslot = timetableEntry.getTimeslot();
		int startHours = timeslot.getStartHours();
		int startMinutes = timeslot.getStartMinutes();
		int endHours = timeslot.getEndHours();
		int endMinutes = timeslot.getEndMinutes();
		if (startHours < 0 || startHours >= 24
				|| startMinutes < 0 || startMinutes >= 60
				|| endHours < 0 || endHours >= 24
				|| endMinutes < 0 || endMinutes >= 60) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "timeslotWithinRange", getObjectLabel(timetableEntry, context) },
						 new Object[] { timetableEntry },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvaluationFormEntry(EvaluationFormEntry evaluationFormEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(evaluationFormEntry, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(evaluationFormEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(evaluationFormEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(evaluationFormEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(evaluationFormEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(evaluationFormEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(evaluationFormEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(evaluationFormEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(evaluationFormEntry, diagnostics, context);
		if (result || diagnostics != null) result &= validateEvaluationFormEntry_weightingWithinRange(evaluationFormEntry, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the weightingWithinRange constraint of '<em>Evaluation Form Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String EVALUATION_FORM_ENTRY__WEIGHTING_WITHIN_RANGE__EEXPRESSION = "self.weighting >= 0 and self.weighting <= 100";

	/**
	 * Validates the weightingWithinRange constraint of '<em>Evaluation Form Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvaluationFormEntry_weightingWithinRange(EvaluationFormEntry evaluationFormEntry, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.EVALUATION_FORM_ENTRY,
				 evaluationFormEntry,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "weightingWithinRange",
				 EVALUATION_FORM_ENTRY__WEIGHTING_WITHIN_RANGE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCreditReduction(CreditReduction creditReduction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(creditReduction, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validateCreditReduction_onCourseOtherThanByCourse(creditReduction, diagnostics, context);
		if (result || diagnostics != null) result &= validateCreditReduction_reductionWithinRange(creditReduction, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the onCourseOtherThanByCourse constraint of '<em>Credit Reduction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CREDIT_REDUCTION__ON_COURSE_OTHER_THAN_BY_COURSE__EEXPRESSION = "self.onCourse <> self.byCourse";

	/**
	 * Validates the onCourseOtherThanByCourse constraint of '<em>Credit Reduction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCreditReduction_onCourseOtherThanByCourse(CreditReduction creditReduction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.CREDIT_REDUCTION,
				 creditReduction,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "onCourseOtherThanByCourse",
				 CREDIT_REDUCTION__ON_COURSE_OTHER_THAN_BY_COURSE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the reductionWithinRange constraint of '<em>Credit Reduction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CREDIT_REDUCTION__REDUCTION_WITHIN_RANGE__EEXPRESSION = "self.reduction >= 0.0 and self.reduction <= self.onCourse.credits";

	/**
	 * Validates the reductionWithinRange constraint of '<em>Credit Reduction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCreditReduction_reductionWithinRange(CreditReduction creditReduction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.CREDIT_REDUCTION,
				 creditReduction,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "reductionWithinRange",
				 CREDIT_REDUCTION__REDUCTION_WITHIN_RANGE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseWork(CourseWork courseWork, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(courseWork, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(courseWork, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(courseWork, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(courseWork, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(courseWork, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(courseWork, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(courseWork, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(courseWork, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(courseWork, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseWork_nonNegativeHours(courseWork, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the nonNegativeHours constraint of '<em>Course Work</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE_WORK__NON_NEGATIVE_HOURS__EEXPRESSION = "self.lectureHours >= 0 and self.labHours >= 0";

	/**
	 * Validates the nonNegativeHours constraint of '<em>Course Work</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseWork_nonNegativeHours(CourseWork courseWork, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CourseswebPackage.Literals.COURSE_WORK,
				 courseWork,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL",
				 "nonNegativeHours",
				 COURSE_WORK__NON_NEGATIVE_HOURS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(person, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoom(Room room, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(room, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudyProgram(StudyProgram studyProgram, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(studyProgram, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEStaffRole(EStaffRole eStaffRole, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateESemester(ESemester eSemester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEWeekDay(EWeekDay eWeekDay, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEClassType(EClassType eClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateETimeslot(Timeslot eTimeslot, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //CourseswebValidator
