/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getContent <em>Content</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getCourseInstances <em>Course Instances</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getRequiredCourses <em>Required Courses</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getRecommendedCourses <em>Recommended Courses</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getCreditReductions <em>Credit Reductions</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getCourseWork <em>Course Work</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Course#getEvaluationForm <em>Evaluation Form</em>}</li>
 * </ul>
 *
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='recommendedCoursesOther requiredCoursesOther courseInstancesDistinctYearSemesterPair evaluationFormWeightingCorrectSum nonNegativeCredits'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL recommendedCoursesOther='self.recommendedCourses-&gt;forAll(rc|rc&lt;&gt;self)' requiredCoursesOther='self.requiredCourses-&gt;forAll(rc|rc&lt;&gt;self)' courseInstancesDistinctYearSemesterPair='self.courseInstances-&gt;forAll(ci1, ci2 | ci1 &lt;&gt; ci2 implies (ci1.year &lt;&gt; ci2.year or ci1.semester &lt;&gt; ci2.semester))' evaluationFormWeightingCorrectSum='self.evaluationForm.weighting-&gt;sum()=100' nonNegativeCredits='self.credits&gt;=0.0'"
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.Course#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(float)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_Credits()
	 * @model
	 * @generated
	 */
	float getCredits();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(float value);

	/**
	 * Returns the value of the '<em><b>Course Instances</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.CourseInstance}.
	 * It is bidirectional and its opposite is '{@link tdt4250.coenmv.coursesweb.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Instances</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_CourseInstances()
	 * @see tdt4250.coenmv.coursesweb.CourseInstance#getCourse
	 * @model opposite="course" containment="true"
	 * @generated
	 */
	EList<CourseInstance> getCourseInstances();

	/**
	 * Returns the value of the '<em><b>Required Courses</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Courses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Courses</em>' reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_RequiredCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getRequiredCourses();

	/**
	 * Returns the value of the '<em><b>Recommended Courses</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recommended Courses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recommended Courses</em>' reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_RecommendedCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getRecommendedCourses();

	/**
	 * Returns the value of the '<em><b>Credit Reductions</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.CreditReduction}.
	 * It is bidirectional and its opposite is '{@link tdt4250.coenmv.coursesweb.CreditReduction#getOnCourse <em>On Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credit Reductions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credit Reductions</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_CreditReductions()
	 * @see tdt4250.coenmv.coursesweb.CreditReduction#getOnCourse
	 * @model opposite="onCourse" containment="true"
	 * @generated
	 */
	EList<CreditReduction> getCreditReductions();

	/**
	 * Returns the value of the '<em><b>Course Work</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Work</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Work</em>' containment reference.
	 * @see #setCourseWork(CourseWork)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_CourseWork()
	 * @model containment="true"
	 * @generated
	 */
	CourseWork getCourseWork();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.Course#getCourseWork <em>Course Work</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Work</em>' containment reference.
	 * @see #getCourseWork()
	 * @generated
	 */
	void setCourseWork(CourseWork value);

	/**
	 * Returns the value of the '<em><b>Evaluation Form</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.EvaluationFormEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluation Form</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation Form</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourse_EvaluationForm()
	 * @model containment="true"
	 * @generated
	 */
	EList<EvaluationFormEntry> getEvaluationForm();

} // Course
