/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.CourseInstance#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.CourseInstance#getResponsibleDepartment <em>Responsible Department</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.CourseInstance#getStaff <em>Staff</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.CourseInstance#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.CourseInstance#getSemester <em>Semester</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.CourseInstance#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourseInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='timetableScheduledLectureHoursMatch timetableScheduledLabHoursMatch timetableStudyProgramsProvideCourse timetableEntriesNotInterfering staffHasOneCourseCoordinator staffNoDuplicateRoles'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL timetableStudyProgramsProvideCourse='self.timetable-&gt;forAll(te|te.studyPrograms-&gt;forAll(sp|sp.courses-&gt;includes(self.course)))' staffHasOneCourseCoordinator='self.staff-&gt;select(s|s.role = EStaffRole::COURSE_COORDINATOR)-&gt;size() = 1' staffNoDuplicateRoles='self.staff-&gt;forAll(s1, s2 | s1 &lt;&gt; s2 implies (s1.person &lt;&gt; s2.person or s1.role &lt;&gt; s2.role))'"
 * @generated
 */
public interface CourseInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Timetable</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.TimetableEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timetable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timetable</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourseInstance_Timetable()
	 * @model containment="true"
	 * @generated
	 */
	EList<TimetableEntry> getTimetable();

	/**
	 * Returns the value of the '<em><b>Responsible Department</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Department</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Department</em>' reference.
	 * @see #setResponsibleDepartment(Department)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourseInstance_ResponsibleDepartment()
	 * @model
	 * @generated
	 */
	Department getResponsibleDepartment();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.CourseInstance#getResponsibleDepartment <em>Responsible Department</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsible Department</em>' reference.
	 * @see #getResponsibleDepartment()
	 * @generated
	 */
	void setResponsibleDepartment(Department value);

	/**
	 * Returns the value of the '<em><b>Staff</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.Staff}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Staff</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Staff</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourseInstance_Staff()
	 * @model containment="true"
	 * @generated
	 */
	EList<Staff> getStaff();

	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourseInstance_Year()
	 * @model
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.CourseInstance#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Semester</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.coenmv.coursesweb.ESemester}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semester</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' attribute.
	 * @see tdt4250.coenmv.coursesweb.ESemester
	 * @see #setSemester(ESemester)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourseInstance_Semester()
	 * @model
	 * @generated
	 */
	ESemester getSemester();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.CourseInstance#getSemester <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' attribute.
	 * @see tdt4250.coenmv.coursesweb.ESemester
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(ESemester value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.coenmv.coursesweb.Course#getCourseInstances <em>Course Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(Course)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getCourseInstance_Course()
	 * @see tdt4250.coenmv.coursesweb.Course#getCourseInstances
	 * @model opposite="courseInstances" transient="false"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.CourseInstance#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

} // CourseInstance
