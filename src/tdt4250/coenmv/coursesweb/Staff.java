/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Staff</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.Staff#getRole <em>Role</em>}</li>
 *   <li>{@link tdt4250.coenmv.coursesweb.Staff#getPerson <em>Person</em>}</li>
 * </ul>
 *
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getStaff()
 * @model
 * @generated
 */
public interface Staff extends EObject {
	/**
	 * Returns the value of the '<em><b>Role</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.coenmv.coursesweb.EStaffRole}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' attribute.
	 * @see tdt4250.coenmv.coursesweb.EStaffRole
	 * @see #setRole(EStaffRole)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getStaff_Role()
	 * @model
	 * @generated
	 */
	EStaffRole getRole();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.Staff#getRole <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' attribute.
	 * @see tdt4250.coenmv.coursesweb.EStaffRole
	 * @see #getRole()
	 * @generated
	 */
	void setRole(EStaffRole value);

	/**
	 * Returns the value of the '<em><b>Person</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person</em>' reference.
	 * @see #setPerson(Person)
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getStaff_Person()
	 * @model
	 * @generated
	 */
	Person getPerson();

	/**
	 * Sets the value of the '{@link tdt4250.coenmv.coursesweb.Staff#getPerson <em>Person</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Person</em>' reference.
	 * @see #getPerson()
	 * @generated
	 */
	void setPerson(Person value);

} // Staff
