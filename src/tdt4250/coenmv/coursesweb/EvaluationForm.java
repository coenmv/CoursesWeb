/**
 */
package tdt4250.coenmv.coursesweb;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation Form</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.coenmv.coursesweb.EvaluationForm#getEntries <em>Entries</em>}</li>
 * </ul>
 *
 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getEvaluationForm()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correctWeightingSum'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL correctWeightingSum='self.entries.weighting-&gt;sum() = 100'"
 * @generated
 */
public interface EvaluationForm extends EObject {
	/**
	 * Returns the value of the '<em><b>Entries</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.coenmv.coursesweb.EvaluationFormEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entries</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entries</em>' containment reference list.
	 * @see tdt4250.coenmv.coursesweb.CourseswebPackage#getEvaluationForm_Entries()
	 * @model containment="true"
	 * @generated
	 */
	EList<EvaluationFormEntry> getEntries();

} // EvaluationForm
